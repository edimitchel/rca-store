#!/bin/sh

# Génère le CSS par le SASS
echo Surveillance des modifications des fichiers SASS pour génération automatique

sass --watch -t compressed application/assets/scss/style.scss:application/assets/css/bootflat/bootflat.min.css