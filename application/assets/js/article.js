;(function(){

    function getConfirmBox(id, finish) {
        var boxConfirm = $("<span/>", {class: "title"}).text('Combien ?');
        var wrap = $("<span/>", {class: "wrap"});

        var less = $("<span/>", {class: "less glyphicon glyphicon-minus"});
        var number = $("<input/>", {type: "number", class: "inNbArt", value: 1, min: 1});
        var more = $("<span/>", {class: "more glyphicon glyphicon-plus"});
        var ok = $("<span/>", {class: "yes"}).text("Valider");

        wrap.append(less).append(number).append(more).append(ok)

        ok.click(function () {
            ajouterArticle(id, parseInt(number.val()));
            finish('accept');
            return false;
        });

        less.click(function () {
            number.val(Math.max(1, parseInt(number.val()) - 1));
            return false;
        });

        more.click(function () {
            number.val(parseInt(number.val()) + 1);
            return false;
        });

        boxConfirm.append(wrap);

        $(window).keydown(function(evt){
            if(evt.keyCode == 27)
                finish('decline');
            else if(evt.keyCode == 13)
                ok.click();
            else if(evt.keyCode == 38)
                more.click();
            else if(evt.keyCode == 40)
                less.click();

        });

        return boxConfirm;
    }

    var isAnimate = false;
    $('.ajout-panier').on('click', function(evt){
        var btnAjout = $(this);
        var btnPanier = $("#panierBtn");
        var posPanier = {
            x : btnPanier.offset().left,
            y : btnPanier.offset().top,
            width: btnPanier.width()
        }, posBtn = {
            x : $(this).offset().left,
            y : $(this).offset().top
        };

        animateToPanier(posBtn, posPanier, function(obj, finish){
            obj.append(getConfirmBox(btnAjout.attr('data-id'), finish));
            obj.find('input').focus();
        });

        return false;
    });

    function animateToPanier(posB, posE, callback){
        if(!isAnimate) {
            var item = $("<div/>", {class: "animationToPanier"});
            item.css('left', posB.x);
            item.css('top', posB.y);
            $("body").append(item);
            isAnimate = true;
            setTimeout(function () {
                item.addClass('toPanier');
                item.css('left', posE.x);
                item.css('top', posE.y);
                item.css('width', posE.width);
            }, 10);

            $('html, body').delay(150).animate({
                scrollTop: 0
            }, 300);
            var once = false;
            item.on('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function(){
                if(!once){
                    callback(item, function(classFinish){
                        item.addClass(classFinish);
                        item.on('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function(){
                            item.remove();
                            isAnimate = false;
                        });
                    });
                    once = true;
                }
            });
        }
    }
})();
