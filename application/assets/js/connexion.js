;(function(){
    $(".connexion-rapide form").submit(function(evt){
        var form = $(this);
        $.post("/ajax/connexion", {
            "mail" : form.find(".cntMail").val(),
            "mdp": form.find(".cntMdp").val()
        }, null, 'json').done(function(data){
            form.find('.error').remove();
            if(data.status){
                window.location.reload();
            } else {
                for(var i in data.error) {
                    form.find('.errors').append("<div class='error'>" + data.error[i] + '</div>');
                }
            }
        });

        evt.preventDefault();
        return false;
    });

    $("aside #formConnexion form").submit(function(evt){
        var form = $(this);
        $.post("/ajax/connexion", {
            "mail" : form.find(".cntMail").val(),
            "mdp": form.find(".cntMdp").val()
        }, null, 'json').done(function(data){
            form.find('.error').remove();
            if(data.status){
                window.location = form.find(".cntUrl").val();
            } else {
                for(var i in data.error) {
                    form.find('.errors').append("<div class='error'>" + data.error[i] + '</div>');
                }
            }
        });

        evt.preventDefault();
        return false;
    })
})();
