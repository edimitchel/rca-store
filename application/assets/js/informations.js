;(function(){

    var $currentModification = null;

    function hasOption(name){
        return $.inArray('cached', $currentModification.options) >= 0
    }

    $('.entry-modification i.edit').click(function(){

        var wrap = $(this).parents('.entry-modification')
        ,   name = wrap.attr('data-name')
        ,   options = wrap.attr('data-option');

        if(typeof options != 'undefined')
            options = options.split(" ");
        else
            options = null;

        var dataEntry = {
            'name': name,
            'node': wrap,
            'options' : options
        };

        editField(dataEntry);
    });

    $('.entry-modification i.submit').click(function(){
        if($currentModification !== null) {
            saveField(function(data){
                $currentModification.node.find('.content span').text(data);
                setCurrentModification(null);
            });
        }
    });

    function setCurrentModification(object){
        if(object === null) {
            // SAVE
            $currentModification.node.removeClass('onmodification');
            $currentModification.node.find('input').remove();

            $currentModification = null;
        } else {
            // EDIT
            $currentModification = object;

            var value = $currentModification.node.find('.content span').text();
            $currentModification.value = value;

            $currentModification.node.find('.content').append($('<input/>', {
                value: $currentModification.value.indexOf('*') >=0 ? "" : $currentModification.value,
                class: "form-control",
                type: hasOption('password') ? 'password' : 'text'
            }));

            $currentModification.node.find('input').focus().keydown(function(evt){
                if(evt.keyCode == 13){
                    $currentModification.node.find('i.submit').click();
                } else if(evt.keyCode == 27){
                    saveField(false);
                }
            });

            $currentModification.node.addClass('onmodification');
        }
    }

    function editField(data){
        if($currentModification === null) {
            setCurrentModification(data);

        }
    }

    function saveField(callback){
        if($currentModification !== null) {
            if(callback === false){
                setCurrentModification(null);
            } else {
                $currentModification.newValue = $currentModification.node.find('input').val();
                if (showCriterias()) {
                    $.post("/ajax/modifierdonnee/" + $currentModification.name, {
                        value: $currentModification.newValue
                    }, null, "json").done(function (data) {
                        if (data.status) {
                            callback(data.data.value);
                        } else {
                            alert(data.error.msg);
                            callback($currentModification.value);
                        }
                    }).fail(function () {
                        alert("Une erreur est survenue durant l'opération d'enregistrement");
                        callback($currentModification.value);
                    });
                } else {
                    $currentModification.node.find('input').val($currentModification.value);
                }
            }
        }
    }

    function showCriterias(){
        if(hasOption('confirm') && confirm("Le mot de passe est le suivant: "+$currentModification.newValue+", vous confirmez ?"))
            return true;

        if($currentModification.newValue == '')
        {
            alert("Ce champ doit être renseigné.");
            return false;
        }
        return true;
    }

})();
