function init() {
	'use strict';
	var form = $('#formInscription');
	
	if (form.length != 0) {
		form.submit(function (event) {
			initError(form);
			var fCivilite = form.find('#formClientCivilite'),
				fNom = form.find('#formClientNom'),
				fPrenom = form.find('#formClientPrenom'),
				fMail = form.find('#formClientMail'),
				fMotdepasse = form.find('#formClientMotdepasse'),
				fVerifMotdepasse = form.find('#formClientVerifMotdepasse'),
				fAdresse = form.find('#formClientAdressePostale'),
				fCP = form.find('#formClientCodePostal'),
				fVille = form.find('#formClientVille');

			if(fMotdepasse.isEmpty())
				addError("motdepasse", "Le mot de passe est obligatoire.");
			else if(fMotdepasse != fVerifMotdepasse)
				addError("motdepasse", "Les mots de passe sont différents");
			addError("motdepasse", "Les mots de passe sont différents");

			var r = showError(form);
			return r;
		});
	}
}
init();

var error = [];
function initError(form){
	error = [];
	form.find('.errorWrap').remove();
}


function addError(type, value) {
	if(error[type])
		error[type] += ', ' + value.substr(0,1).toLowerCase()+value.substr(1);
	else
		error[type] = value;
}

function showError(form) {
	var err = $('<p />', { class:'errorWrap' });
	if(error.length == 0) return true;
	for(var i in error){
		var e = error[i],
			el = form.find('[data-type='+i+']');
		el.append(err.clone().text(e));
	}
	return false;
}

String.prototype.isEmpty = function () {
	'use strict';
	return this === null || this === '';
};