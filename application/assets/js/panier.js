;(function(){

    $('button.deletePanier').click(function(){
        supprimerPanier();
    });
    $('button.savePanier').click(function(){
        enregistrerPanier();
    });
    $('button.delete').click(function(){
        var line = $(this).parents("tr");
        var idArticle = parseInt(line.find('.ref').text());
        supprimerArticle(idArticle, 0, function(){
            line.remove();
            window.location.reload();
        });
    });
    $('button.add').click(function(){
        var line = $(this).parents("tr");
        var prixTotal = $("#prixTotal strong");
        var idArticle = parseInt(line.find('.ref').text());
        ajouterArticle(idArticle, 1, function(data){
            var c = line.find('.count');
            c.text(parseInt(c.text()) + 1);
            var t = line.find('.total');
            t.text(parseFloat(parseInt(c.text()) * line.find('.prixUnite').text()).toFixed(2));

            prixTotal.text(data.price);
        });
    });
    $('button.remove').click(function(){
        var line = $(this).parents("tr");
        var prixTotal = $("#prixTotal strong");
        var idArticle = parseInt(line.find('.ref').text());
        supprimerArticle(idArticle, 1, function(data){
            var c = line.find('.count');
            if(parseInt(c.text()) <= 1){
                line.remove();
                if($('tbody tr').length == 0)
                    window.location.reload();
            } else {
                c.text(parseInt(c.text()) - 1);
                var t = line.find('.total');
                t.text(parseFloat(parseInt(c.text()) * line.find('.prixUnite').text()).toFixed(2));
            }

            prixTotal.text(data.price);
        });
    });

    $('#reglementForm').submit(function(){
        return confirm($(this).attr('data-messageConfirm'));
    })
})();

var typePanier = document.body.className;

if(typePanier != 'panier' && typePanier != 'compte')
    typePanier = 'panier';

var souhaitId = null;

console.log(typePanier);

if($("#panierTable").length > 0 && typePanier == "compte"){
    souhaitId = $("#panierTable").attr('data-id');
}

function debugError(){
    console.log(arguments);
}

function supprimerPanier(){
    var params = {};
    if(souhaitId !== null)
        params.souhaitid = souhaitId;

    $.post("/ajax/supprimer"+typePanier+"/", params, null, 'json').done(function(data){
        if(data.status){
            if(souhaitId !== null)
                window.location.reload();
            else
                window.location = "/compte/souhaits";
        }
    }).fail(debugError);
}

function enregistrerPanier(){
    $.post("/ajax/enregistrerpanier/", null, null, 'json').done(function(data){
        if(data.status){
            window.location = "/compte/souhait/"+data.data.idsouhait;
        }
    }).fail(debugError);
}

function supprimerArticle(id, count, callback){
    var params = {};
    if(souhaitId !== null)
        params.souhaitid = souhaitId;

    $.post("/ajax/supprimerarticle"+typePanier+"/"+id+"/"+count, params, null, 'json').done(function(data){
        if(data.status){
            callback(data.data);
        }
    }).fail(debugError);
}

function ajouterArticle(id, count, callback){
    var params = {};
    if(souhaitId !== null)
        params.souhaitid = souhaitId;

    if(isNaN(count))
        count = 1;

    var panier = $("#panierBtn").find("a");
    var nbArt = panier.find('.nb');
    $.post("/ajax/ajouterarticle"+typePanier+"/"+id+"/"+count, params, null, 'json').done(function(data){
        if(data.status){
            var newPrice = data.data.price;

            if(!$("body").hasClass("panier")) {
                if (nbArt.length == 1) {
                    // Nombre présent
                    nbArt.text(newPrice);
                } else {
                    // Nombre absent
                    panier.append($('<span/>', {class: "nb", text: newPrice}));
                }
            }
            if(callback)
               callback(data.data);
        }
    }).fail(debugError);
}
