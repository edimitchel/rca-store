;(function(){

    $('#paiementForm').submit(function(evt){
        var form = $(this);
        var url = $(this).attr("action");
        form.find('button').attr('disabled','disabled');
        payerCommande(url, form.find('#montant').val(), form.find('#devise').val(), function(data){
            form.find('button').removeClass('btn-default').addClass('btn-success');
            var message = data.msg
            $.post("/ajax/confirmerpaiement/", {
                'idCommande' : form.find('#idCommande').val()
            }, null, 'json').done(function(data){
                if(data.status) {
                    $(".text-callback").html(message).addClass("text-success");
                    form.find('button').text('Commande finalisée');
                }
            });
        });
        evt.preventDefault();
        return false;
    });
})();


function payerCommande(url, montant, devise, callback){
    $.post(url, {
        montant : montant,
        devise : devise
    }, null, 'json').done(function(data){
        if(data.status) {
            callback(data.data);
        } else {
            alert("Une erreur s'est déroulé durant le paiement.");
        }
    });
}
