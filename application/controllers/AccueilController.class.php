<?php

/**
* 
*/
class AccueilController extends BaseController
{

	public function index(){
		$this->attach('bestarticles', AccueilController::getBestArticles());

		$this->attach('page_description', "RCA Store est la meilleure boutique en ligne d'avions à modèles réduits.");
	}

    /**
     * @return \Propel\Runtime\Collection\ObjectCollection
     */
    public function getBestArticles(){
        $query = "SELECT id_article FROM commande_article WHERE id_commande IN (SELECT id FROM commande WHERE paye = 1) GROUP BY id_article ORDER BY sum(quantite) DESC LIMIT 6;";
        $result = \Propel\Runtime\Propel::getConnection()->query($query);

        $articlesArr = array();

        while($a = $result->fetch()){
            array_push($articlesArr, $a[0]);
        }

        $articles = ArticleQuery::create()->filterById($articlesArr)->find();

        return $articles;
    }
}