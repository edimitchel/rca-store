<?php

class AjaxController extends BaseController
{
    protected function configure()
    {
        $this->setIsDataController(true);
    }


    public function index(){
        $this->getResponse()->setError("action", "Les opérations AJAX ne peuvent opérées à la racine.");
    }

    /**
     * Ajoute un article au panier
     * @param $itemId
     * @param int $count
     */
    public function _ajouterarticlepanier($itemId, $count = 1){
        $panier = Panier::getPanier(false);
        $panier->addItem($itemId, $count);
        $panier->save();

        $this->getResponse()->setData("msg", "L'article numéro n°" . $itemId . " a été ajouté " . $count . " fois.");
        $this->getResponse()->setData("price", $panier->getPrixTotal());
    }

    /**
     * Supprime la panier
     */
    public function _supprimerpanier(){
        (new Panier())->save();
    }

    /**
     * Supprime un ou plusieurs articles du panier
     * @param $itemId
     * @param int $count
     */
    public function _supprimerarticlepanier($itemId, $count = 0){
        $panier = Panier::getPanier(false);
        $panier->removeItem($itemId, $count);
        $panier->save();

        $this->getResponse()->setData("price", $panier->getPrixTotal());
    }

    /**
     * Enregistre le panier en tant que liste de souhaits
     * @throws Exception
     */
    public function _enregistrerpanier(){
        $panier = Panier::getPanier(false);

        if(($idClient = CompteController::getClientId()) !== false){
            $souhait = $panier->saveOnBase(Panier::LISTESOUHAIT, $idClient);
            $this->getResponse()->setData("idsouhait", Panier::encryptId($souhait->getId()));
        } else {
            $this->getResponse()->setError("user", "Cette fonction est disponible qu'en étant connecté.");
        }
    }

    /**
     * Ajoute un article à la liste de souhait
     * @param $itemId
     * @param int $count
     * @throws Exception
     */
    public function _ajouterarticlecompte($itemId, $count = 1){
        $listeId = Request::post('idsouhait');

        $panier = Panier::getPanier($listeId, CompteController::getClientId());
        if($panier !== false){
            $panier->addItem($itemId, $count);
            $panier->updateOnBase(Panier::LISTESOUHAIT, CompteController::getClientId());

            $this->getResponse()->setData("msg", "L'article numéro n°" . $itemId . " a été ajouté " . $count . " fois.");
            $this->getResponse()->setData("price", $panier->getPrixTotal());
        } else {
            $this->getResponse()->setStatus(false);
        }

    }

    /**
     * Supprime un ou plusieurs articles de la liste de souhaits
     * @param $itemId
     * @param int $count
     * @throws Exception
     */
    public function _supprimerarticlecompte($itemId, $count = 1){
        $listeId = Request::post('idsouhait');

        $panier = Panier::getPanier($listeId, CompteController::getClientId());
        $panier->removeItem($itemId, $count);

        if($panier->getListItem().length == 0)
            $panier->removeOnBase();
        else
            $panier->updateOnBase(Panier::LISTESOUHAIT);
        $this->getResponse()->setData("price", $panier->getPrixTotal());
    }

    /**
     * Supprime la liste de souhaits
     */
    public function _supprimercompte(){
        $listeId = Request::post('idsouhait');

        $panier = Panier::getPanier($listeId);
        $panier->setUserId(CompteController::getClientId());
        $panier->removeOnBase();
    }

    /**
     * Confirme le paiement en retournant le numéro de transaction bancaire
     * @throws Exception
     */
    public function _confirmerpaiement(){
        $id = Request::post('idCommande');
        Panier::getPanier($id, CompteController::getClientId())->saveOnBase(Panier::COMMANDETERMINEE);
        Panier::removePanier();

        $this->getResponse()->setStatus(true);
    }


    /**
     * Établie la connexion d'un utilisateur
     */
    public function _connexion(){
        $mail = Request::post("mail");
        $mdp = Request::post("mdp");

        $r = IdentificationTask::getTask()->connect($mail, $mdp);

        if($r){
            $this->getResponse()->setStatus(true);
        } else {
            $this->getResponse()->setError("identifiant","Identifiant ou mot de passe erroné(s).");
        }
    }

    /**
     * Modifie une données cliente
     * @param string $field
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function _modifierDonnee($field = false){
        $value = Request::post("value");
        if($field === false || $value === false){
            $this->getResponse()->setError("champs", "Le champ et la valeur sont à renseigner");
        } else {
            $client = CompteController::getClient();
            $ok = false;

            if(method_exists($client,'set'.ucfirst($field))){
                if($client->{'get'.ucfirst($field)}() == $value)
                    $ok = true;
                else
                    $client->{'set'.ucfirst($field)}($value);
            } else {
                $this->getResponse()->setError("msg", "L'enregistrement a échoué.");
                return;
            }

            if($client->save() == 1 && $ok == false)
                $ok = true;

            if($ok){
                $newValue = $client->{'get'.ucfirst($field)}($value);
                if($field == "motdepasse")
                    $newValue = str_repeat("*", strlen($value));
                $this->getResponse()->setData("value", $newValue);
            } else {
                $this->getResponse()->setError("msg", "L'enregistrement a échoué.");
            }
        }

    }

}