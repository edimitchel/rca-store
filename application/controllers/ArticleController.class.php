<?php

class ArticleController extends BaseController
{

    public function index(){
        FrontController::redirect("/catalogue");
    }

    /**
     * Affiche un article
     * @param int $id
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function _show($id = false){
        if($id == false){
            FrontController::redirect("/catalogue");
            die;
        } else {
            $id = intval($id);
        }
        $article = ArticleQuery::create()->findOneById($id);
        $this->attach("article", $article);

        $this->attach('page_description', $article->getDescription());


        $sameArticles = ArticleQuery::create()->filterById($id, '!=')->filterByCategorie($article->getCategorie());
        $this->attach("sameArticles", $sameArticles);

        $this->attach('page_description', "RCA Store est la meilleure boutique en ligne d'avions à modèles réduits.");
    }
}