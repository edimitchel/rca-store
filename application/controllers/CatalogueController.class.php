<?php

/**
* 
*/
class CatalogueController extends BaseController
{
	public function index(){
    }

    /**
     * Affiche les articles des catégories
     * @param $id
     * @param int $childid
     */
	public function _categorie($id, $childid = false){
		$categorie = CategorieQuery::create()
			->findOneById($id);

        $currentId = $id;

        $childCategorie = false;

        if($childid !== false){
            $childCategorie = CategorieQuery::create()
                ->findOneById($childid);
            $currentId = $childid;
        }

        $this->attach('current_categorie', intval($currentId));

        $articlesArray = ArticleQuery::create()
            ->orderByPrix(\Propel\Runtime\ActiveQuery\Criteria::DESC)
            ->findByIdCategorie($currentId);
        if($childid === false){
            $articlesArray = array();
            $sql = "select a.* from article a, categorie pc, categorie cat where (a.id_categorie = cat.id OR a.id_categorie = pc.id) AND cat.parent = pc.id AND pc.id = :id";
            $con = \Propel\Runtime\Propel::getConnection();

            $stmt = $con->prepare($sql);
            $stmt->bindValue("id", $id);

            $stmt->execute();
            while($rs = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $a = new Article();
                $a->setId($rs->id);
                $a->setLibelle($rs->libelle);
                $a->setPrix(round($rs->prix));
                $a->setMiniature($rs->miniature);
                $a->setDescription($rs->description);
                array_push($articlesArray, $a);
            }

            $categorie = CategorieQuery::create()->findOneById($id);
        }
        $this->attach('articles', $articlesArray);

        // Sidebar menu Catégories
        $this->attach('id_categorie', intval($id));
        $this->attach('id_child_categorie', $childid);
        $this->attach('categories', CatalogueController::getCategories());
        $this->attach('child_categories', CatalogueController::getChildCategories($id));

        if($childid !== false) {
            $this->attach('titre', $categorie->getLibelle() . " / " . $childCategorie->getLibelle());
            $this->attach('titreLink', "<a href=\"/catalogue/categorie/" . $categorie->getId() . "\">" . $categorie->getLibelle() . "</a>" . " / " . $childCategorie->getLibelle());
        } else {
            $this->attach('titre', $categorie->getLibelle());
            $this->attach('titreLink', $categorie->getLibelle());
        }
	}

    /**
     * Affiche les articles correspondant à la recherche avec une pagination
     * @param $query
     * @param int $page
     */
    public function _recherche($query, $page = 1){

        $this->attach('query', $query);
        $this->attach('nCat', $query);

        $count = ArticleQuery::create()
            ->distinct()
            ->addOr(\Map\ArticleTableMap::COL_LIBELLE, "%" . $query . "%", ArticleQuery::LIKE)
            ->addOr(\Map\ArticleTableMap::COL_DESCRIPTION, "%" . $query . "%", ArticleQuery::LIKE)
            ->count();

        $articles = ArticleQuery::create()
            ->distinct()
            ->addOr(\Map\ArticleTableMap::COL_LIBELLE, "%" . $query . "%", ArticleQuery::LIKE)
            ->addOr(\Map\ArticleTableMap::COL_DESCRIPTION, "%" . $query . "%", ArticleQuery::LIKE)
            ->offset(($page-1) * 8)
            ->limit(8)
            ->find();

        $this->attach("count", $count);
        $this->attach("articles", $articles);

        if($count > $page * 8 ) {
            $this->attach("moreResult", true);
            if($page > 1)
                $this->attach("prevPage", "/catalogue/recherche/".$query."/".($page-1));
            $this->attach("nextPage", "/catalogue/recherche/".$query."/".($page+1));
        }
    }

    /**
     * Récupère les catégories
     * @return Categorie[]|\Propel\Runtime\Collection\ObjectCollection
     */
    public static function getCategories(){
        $categories = CategorieQuery::create()
            ->findByParent(null);

        return $categories;
    }

    /**
     * Récupère les catéories filles de la catégorie $id
     * @param $id
     * @return Categorie[]|\Propel\Runtime\Collection\ObjectCollection
     */
    public static function getChildCategories($id){
        $categories = CategorieQuery::create()
            ->findByParent($id);

        return $categories;
    }
}