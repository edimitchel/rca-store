<?php

class CompteController extends BaseController
{

    private $client;

    /**
     * Si le client n'est pas connecté, on redirige vers l'accueil. Sinon, on récupère les données du client
     */
    protected function configure()
    {
        if(FrontController::getSessionData("useronline") === false){
            FrontController::redirect("/");
            die;
        } else {
            $this->client = FrontController::getSessionData("useronline")->client;
        }
    }

    /**
     * Récupère l'identifiant du client connecté
     *
     * @return int L'identifiant du client. <code>null</code> si le client n'est pas connecté.
     */
    public static function getClientId(){
        $client = self::getClient();
        if($client == null)
            return null;
        return $client->getId();
    }

    /**
     * Récupère le client connecté
     *
     * @return Client L'objet client. <code>null</code> si le client n'est pas connecté.
     */
    public static function getClient(){
        if(FrontController::getSessionData("useronline") === false){
            return null;
        } else {
            return FrontController::getSessionData("useronline")->client;
        }
    }

    /**
     * Renvoie l'existence ou non d'une session utilisateur
     *
     * @return bool Un utilisateur est connecté
     */
    public static function isConnected(){
        return self::getClientId() !== null;
    }


    public function index(){
        FrontController::redirect('/compte/informations');
    }

    /**
     * Affiche les listes de souhait
     */
    public function _souhaits(){
        $listeSouhaits = CommandeQuery::create()->filterByDate(null)->findByIdClient($this->client->getId());
        $souhaits = array();

        foreach($listeSouhaits AS $souhait){
            array_push($souhaits, Panier::getPanier(Panier::encryptId($souhait->getId()), $this->client->getId()));
        }

        $this->attach("souhaits", $souhaits);
    }

    /**
     * Affiche le souhait en détail
     * @param string $id
     */
    public function _souhait($id = false){
        if($id == false)
            FrontController::redirect("/compte/souhaits");

        $souhait = Panier::getPanier($id, $this->client->getId());

        if($souhait == false)
            FrontController::redirect("/compte/souhaits");

        $this->attach("listesouhait", $souhait);
    }

    /**
     * Affiche les commandes en attente et terminées
     */
    public function _commandes(){
        $listeCommande = CommandeQuery::create()->filterByPaye(true)->_or()->filterByDate(null,\Propel\Runtime\ActiveQuery\Criteria::NOT_EQUAL)->orderByDate(\Propel\Runtime\ActiveQuery\Criteria::DESC)->findByIdClient($this->client->getId());
        $commandes = array();

        foreach($listeCommande AS $commande){
            array_push($commandes, Panier::getPanier(Panier::encryptId($commande->getId()), $this->client->getId()));
        }

        $this->attach("commandes", $commandes);
    }

    /**
     * Affiche la page informations
     */
    public function _informations(){

        $this->attach('page_description', "Information sur votre compte");
    }
}