<?php

class IdentificationController  extends BaseController
{

    /**
     * Affiche la page inscription
     */
    public function index(){
        $this->display("inscription");
    }

    /**
     * Effectue la connexion d'un internaute
     */
    public function _connect(){
        $url = Request::post("url");

        $mail = Request::post("mail");
        $mdp = Request::post("mdp");

        $r = IdentificationTask::getTask()->connect($mail, $mdp);

        if($r){
            FrontController::redirect($url);
        } else {
            FrontController::redirect("/identification");
        }
    }

    /**
     * Déconnecte l'utilisateur actif
     */
    public function _disconnect(){
        IdentificationTask::getTask()->disconnect();
    }

}