<?php

/**
* 
*/
class InscriptionController extends BaseController
{

	private $civilite = "";
	private $nom = "";
	private $prenom = "";
	private $adressemail = "";
	private $motdepasse = "";
	private $verifmdp = "";
	private $adressepostale = "";
	private $codepostal = "";
	private $ville = "";

	/**
	 * Affiche le formulaire d'inscription
	 */
	public function index(){
		$this->attach('page_description', "Page d'inscription pour rejoindre la communauté RCA Store");
	}

	/**
	 * Page de soumission et de traitement
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function _submit(){
		$erreurs = array();

		$this->civilite = Request::post("civilite");
		$this->nom = Request::post("nom");
		$this->prenom = Request::post("prenom");
		$this->adressemail = Request::post("adressemail");
		$this->motdepasse = Request::post("motdepasse");
		$this->verifmdp = Request::post("verifMdp");
		$this->adressepostale = Request::post("adressepostale");
		$this->codepostal = Request::post("codepostal");
		$this->ville = Request::post("ville");

		if(empty($this->nom)){
			$erreurs["nom"] = "Le nom est à renseigner";
		}

		if(empty($this->prenom)){
			$erreurs["prenom"] = "Le prénom est à renseigner";
		}

		if(empty($this->adressemail)){
			$erreurs["mail"] = "L'adresse mail est à renseigner";
		} elseif (!filter_var($this->adressemail, FILTER_VALIDATE_EMAIL)){
			$erreurs["mail"] = "L'adresse mail est incorrecte";
		}

		if(empty($this->motdepasse) || strlen($this->motdepasse) < 6){
			$erreurs["motdepasse"] = "Le mot de passe à renseigner doit au minimum faire 6 caractères.";
		} elseif($this->motdepasse != $this->verifmdp) {
			$erreurs["motdepasse"] = "Les mots de passe ne correspondent pas.";
		}


		if(empty($erreurs)){
			$client = new Client;
			$client->setCivilite($this->civilite);
			$client->setNom($this->nom);
			$client->setPrenom($this->prenom);
			$client->setMail($this->adressemail);
			$client->setMotdepasse($this->motdepasse);
			$client->setAdresse($this->adressepostale);
			$client->setCodepostal($this->codepostal);
			$client->setVille($this->ville);

			$r = $client->save();

			if($r){
				IdentificationTask::getTask()->connect($this->adressemail, $this->motdepasse);

				$this->attach("newuser", $client);
				$this->display("inscription_reussite");
			} else {
				$erreurs["operation"] = "Une erreur est survenu à l'enregistrement de votre compte.";
			}
		}

		if(!empty($erreurs)){
			$this->attach("civilite" , $this->civilite);
			$this->attach("nom" , $this->nom);
			$this->attach("prenom" , $this->prenom);
			$this->attach("adressemail" , $this->adressemail);
			$this->attach("motdepasse" , $this->motdepasse);
			$this->attach("verifmotdepasse" , $this->verifmdp);
			$this->attach("adressepostale" , $this->adressepostale);
			$this->attach("codepostal" , $this->codepostal);
			$this->attach("ville" , $this->ville);

			$this->attach("errors", $erreurs);

			$this->display("inscription");
		}
	}

}