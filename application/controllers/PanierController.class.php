<?php

/**
* 
*/
class PanierController extends BaseController
{
	/**
	 * Affiche le panier
	 */
	public function index(){
		$panier = Panier::getPanier(false);
		$this->attach("panier", $panier);

		$this->attach('page_description', "Le détail de votre panier");
	}
}