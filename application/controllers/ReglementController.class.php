<?php

/**
 *
 */
class ReglementController extends BaseController
{

	const TYPE_REGLEMENT_PANIER = "panier";

	const TYPE_REGLEMENT_SOUHAIT = "souhait";

	private $typeCommande;

	/**
	 * Affiche la page de règlement
	 */
	public function index()
	{
		$origin = Request::post("origin");
		$idSouhait = Request::post("idSouhait");

		if ($origin === false) {
			$origin = "/";
		}

		switch ($typeCommande = Request::post("typeCommande")) {
			case self::TYPE_REGLEMENT_PANIER:
			case self::TYPE_REGLEMENT_SOUHAIT:
				$this->typeCommande = $typeCommande;
				break;
			default:
				$this->typeCommande = false;
		}

		$this->doReglement($origin, $idSouhait);
	}

	/**
	 * Affiche la page de règlement d'une commande spécifique
	 * @param $idCommande
	 */
	public function _finish($idCommande){
		$origin = "/compte/commandes";
		$this->typeCommande = self::TYPE_REGLEMENT_SOUHAIT;
		$this->doReglement($origin, $idCommande);
	}

	/**
	 * Effectue l'opération logique du règlement
	 * @param $origin
	 * @param $id
	 * @throws Exception
	 */
	private function doReglement($origin, $id) {
		if ($this->typeCommande == false) {
			FrontController::redirect($origin);
		} else {
			if (CompteController::isConnected()) {
				$commande = null;
				if ($this->typeCommande == self::TYPE_REGLEMENT_SOUHAIT) {
					if ($id == false)
						FrontController::redirect($origin);
					$commande = Panier::getPanier($id, CompteController::getClientId());
				} else if ($this->typeCommande == self::TYPE_REGLEMENT_PANIER) {
					$comDB = Panier::getPanier(false)->saveOnBase(Panier::LISTESOUHAIT, CompteController::getClientId());
					$commande = Panier::getPanier(Panier::encryptId($comDB->getId()), CompteController::getClientId());
				}

				if ($commande !== null AND $commande instanceof Panier) {
					// On affiche les informations de la commande

					$commande->saveOnBase(Panier::COMMANDEENCOURS, CompteController::getClientId());
					$this->attach("commande", $commande);
					$this->attach("articles", $commande->getArticles());

					$this->display("reglement");

				} else {
					FrontController::redirect($origin);
				}
			} else {
				$this->attach("origin", $origin);
				$this->attach("typeCommande", $this->typeCommande);

				$this->display("reglement_identification");
			}
		}
	}

}