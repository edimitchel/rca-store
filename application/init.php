<?php
	
	require_once APP_PATH . "vendor/autoload.php";

	require_once SYSTEM_PATH . "database/propel/generated-conf/config.php";

	// Désactive le pooling d'instance
	\Propel\Runtime\Propel::disableInstancePooling();