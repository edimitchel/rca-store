<?php

class Panier {

    const INDEX_PANIER = "panier";

    const INDEX_DATE = "date";

    const INDEX_LIST = "listItem";

    private $date;

    private $userId = null;

    private $userSavedId = null;

    private $listItem = array();

    private $userSaved = false;

    private $price = null;

    private $priceAvecTaxe = null;

    private $paye = false;

    const TVA = 20;

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @param null $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return null
     */
    public function getUserSavedId()
    {
        return $this->userSavedId;
    }

    /**
     * @param null $userSavedId
     */
    public function setUserSavedId($userSavedId)
    {
        $this->userSavedId = $userSavedId;
    }

    /**
     * @return array
     */
    public function getListItem()
    {
        return $this->listItem;
    }

    /**
     * @param array $listItem
     */
    public function setListItem($listItem)
    {
        $this->listItem = $listItem;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getItem($id){
        return $this->getListItem()[$id];
    }

    /**
     * @param $itemId
     * @param int $count
     */
    public function addItem($itemId, $count = 1)
    {
        if(isset($this->listItem[$itemId])){
            $this->listItem[$itemId] += $count;
        } else {
            $this->listItem[$itemId] = $count;
        }
    }

    /**
     * @param $itemId
     * @param int $count
     */
    public function removeItem($itemId, $count = 1){
        if(isset($this->listItem[$itemId])){
            if($this->listItem[$itemId] - $count > 0 && $count != 0)
               $this->listItem[$itemId] -= $count;
            else
                unset($this->listItem[$itemId]);
        }
    }

    /**
     *
     */
    public function removeAllItem(){
        $this->listItem = array();
    }

    /**
     * @return boolean
     */
    public function isPaye()
    {
        return $this->paye;
    }

    /**
     * @return boolean
     */
    public function getEtat()
    {
        return $this->isPaye() ? "Terminé" : "En attente de paiement";
    }

    /**
     * @param boolean $paye
     */
    public function setPaye($paye)
    {
        $this->paye = $paye;
    }

    /**
     * @return boolean
     */
    public function isUserSaved()
    {
        return $this->userSaved;
    }

    /**
     * @param boolean $userSaved
     */
    public function setUserSaved($userSaved)
    {
        $this->userSaved = $userSaved;
    }

    /**
     * Récupère le coût du panier
     */
    public function getPrixTotal(){
        $article = ArticleQuery::create()->filterById(array_keys($this->listItem))->find();
        $this->price = 0;
        foreach($article AS $a){
            $this->price += $a->getPrix() * $this->getItem($a->getId());
        }
        return $this->price;
    }

    /**
     * Récupère le coût du panier avec la TVA
     */
    public function getPrixTotalAvecTaxe(){
        $this->price = $this->getPrixTotal();
        $this->priceAvecTaxe = $this->price + round($this->price * (self::TVA / 100), 2);
        return $this->priceAvecTaxe;
    }

    /**
     * @return float
     */
    public function getMontantTVA(){
        return round($this->getPrixTotal() * (self::TVA / 100), 2);
    }

    /**
     * @param $id
     * @return string
     */
    public static function encryptId($id){
        return substr(sha1($id), 0, 6);
    }

    /**
     * @return int
     */
    public function countItem(){
        $i = 0;
        foreach($this->listItem AS $item => $count){
            $i += $count;
        }
        return $i;
    }

    /**
     * Récupère le panier stocké soit dans la session, soit dans la base de données
     *
     * @param $userSaved
     * @return Panier
     */
    public static function getPanier($userSaved, $userid = null){
        if($userSaved !== false){
            if($userid !== null) {
                $listeSouhait = null;
                $listesSouhait = CommandeQuery::create(false)->findByIdClient($userid);
                if($listesSouhait == null)
                    return false;

                foreach($listesSouhait AS $liste) {
                    if(Panier::encryptId($liste->getId()) === $userSaved){
                        $listeSouhait = $liste;
                        break;
                    }
                }

                if($listeSouhait === null)
                    return false;

                $souhait = new Panier;
                $souhait->setUserId($userid);
                $souhait->setUserSaved(true);
                $souhait->setUserSavedId($userSaved);
                $souhait->setPaye($listeSouhait->isPaye());
                $souhait->setDate($listeSouhait->getDate("d/m/Y à H\hi"));

                foreach ($listeSouhait->getCommandeArticlesJoinArticle() AS $arti) {
                    $souhait->addItem($arti->getIdArticle(), $arti->getQuantite());
                }

                return $souhait;
            }
            return false;
        } else {
            $p = FrontController::getSessionData(self::INDEX_PANIER);

            if($p != false){
                $panier = new Panier();
                $panier->setDate($p[self::INDEX_DATE]);
                $panier->setListItem($p[self::INDEX_LIST]);
                $panier->setUserSaved(false);

                return $panier;
            }
            return new Panier();
        }
    }

    /**
     * Sauvegarde le panier dans la session
     */
    public function save(){
        if(!$this->userSaved){
            FrontController::setSessionData(self::INDEX_PANIER, array(self::INDEX_DATE => $this->date, self::INDEX_LIST => $this->listItem));
        }
    }

    /**
     * Supprime le panier
     */
    public static function removePanier(){
        (new Panier())->save();
    }

    // Les différents état d'une commade
    const LISTESOUHAIT = 1;
    const COMMANDEENCOURS = 2;
    const COMMANDETERMINEE = 3;

    /**
     * Sauvegarde le panier/liste/commande en base
     * @param int $status
     * @param null $idClient
     * @return Commande|mixed|null
     * @throws Exception
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function saveOnBase($status = self::LISTESOUHAIT, $idClient = null){
        $commande = null;
        if($this->userId === null && $idClient != null)
            $this->userId = $idClient;
        else if($this->userId === null) {
            throw new Exception("L'identifiant client est obligatoire pour effecter cette action.");
        }

        if($this->isUserSaved() === false) {
            $commande = new Commande;
            $commande->setIdClient($this->userId);
        } else {
            $commandes = CommandeQuery::create()->findByIdClient($this->userId);
            foreach($commandes as $c){
                if(Panier::encryptId($c->getID()) == $this->getUserSavedId()){
                    $commande = $c;
                    break;
                }
            }

            if($commande != null && $status != self::COMMANDETERMINEE){
                CommandeArticleQuery::create()->filterByIdCommande($commande->getId())->delete();
            }
        }

        if($commande != null) {
            switch ($status) {
                case self::LISTESOUHAIT:
                    $commande->setDate(null);
                    $commande->setPaye(false);
                    break;
                case self::COMMANDEENCOURS:
                    $commande->setDate(time());
                    $commande->setPaye(false);
                    break;
                case self::COMMANDETERMINEE:
                    $commande->setDate(time());
                    $commande->setPaye(true);
                    break;
            }

            $commande->save();

            if($status != self::COMMANDETERMINEE) {
                foreach ($this->getArticles() AS $art) {
                    $a = new CommandeArticle;
                    $a->setIdArticle($art->getId());
                    $a->setIdCommande($commande->getId());
                    $a->setQuantite($art->getCount());
                    $a->save();
                }
            }
        } else {
            FrontController::redirect("/");
            die;
        }

        return $commande;
    }

    /**
     * Met à jour les données de la liste/commande
     * @param null $idClient
     * @return Commande|mixed|null
     * @throws Exception
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function updateOnBase($idClient = null){
        $commande = null;
        if($this->userId === null && $idClient != null)
            $this->userId = $idClient;
        else if($this->userId === null) {
            throw new Exception("L'identifiant client est obligatoire pour effecter cette action.");
        }

        $commandes = CommandeQuery::create()->findByIdClient($this->userId);
        foreach($commandes as $c){
            if(Panier::encryptId($c->getID()) == $this->getUserSavedId()){
                $commande = $c;
                break;
            }
        }

        if($commande != null){
            CommandeArticleQuery::create()->filterByIdCommande($commande->getId())->delete();
        }

        foreach ($this->getArticles() AS $art) {
            $a = new CommandeArticle;
            $a->setIdArticle($art->getId());
            $a->setIdCommande($commande->getId());
            $a->setQuantite($art->getCount());
            $a->save();
        }

        return $commande;
    }

    /**
     * Supprime la liste/commande en base
     * @return bool
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function removeOnBase(){
        $commande = null;
        $commandes = CommandeQuery::create()->findByIdClient($this->userId);
        foreach($commandes as $c){
            if(Panier::encryptId($c->getID()) == $this->getUserSavedId()){
                $commande = $c;
                break;
            }
        }

        if($commande != null){
            CommandeArticleQuery::create()->filterByIdCommande($commande->getId())->delete();
            $commande->delete();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Retourne les articles du panier ou de la liste
     * @return array
     */
    public function getArticles(){
        $articlesR = array();
        $articles = ArticleQuery::create()->filterById(array_keys($this->listItem))->find();
        $this->price = 0;
        foreach($articles AS $a){
            $a->setCount($this->getItem($a->getId()));
            array_push($articlesR, $a);
        }
        return $articlesR;
    }
}