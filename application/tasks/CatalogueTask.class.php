<?php

class CatalogueTask extends Task {

    /**
     * Attache la liste des catégorie au template
     * @param BaseController $context
     */
    function task(BaseController $context)
    {
        $context->attach('categories', CatalogueController::getCategories());

    }

}