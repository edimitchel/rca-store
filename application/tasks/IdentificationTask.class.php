<?php

class IdentificationTask extends Task {

    /**
     * Attache différents informations au template pour décrire l'utilisateur connecté
     * @param BaseController $context
     */
    function task(BaseController $context)
    {
        $d = FrontController::getSessionData("useronline");
        $isConnected = $d && is_object($d) && $d->isConnected;

        $context->attach("isConnected", $isConnected);
        if($isConnected) {
            $client = FrontController::getSessionData("useronline")->client;

            $bonjour = "Bonjour";
            if(date('G') > 18 || date('G') < 2)
                $bonjour = "Bonsoir";

            $context->attach("user", (object) array(
                "name"      => $bonjour . " " .$client->getPrenom() . " " . $client->getNom(),
                "client"    => $client
            ));
        }
    }

    /**
     * Enregistre la session de l'utilisateur
     * @param $mail
     * @param $password
     * @return bool
     */
    function connect($mail, $password){
        $c = ClientQuery::create()->findOneByMail($mail);
        if($c !== null && $c->isMotdepasseSame($password)){
            FrontController::setSessionData("useronline", (object) array(
                "isConnected" => true,
                "client" => $c
            ));
            return true;
        }
        return false;
    }

    /**
     * Supprime la session de l'utilisateur
     */
    function disconnect(){
        FrontController::removeSessionData("useronline");
        FrontController::redirect("/");
    }

}