<?php

class PanierTask extends Task {

    /**
     * Ajoute les tâches filles
     */
    protected function preTask()
    {
        $this->addChild(SessionPanierTask::getTask());
    }

    /**
     *
     * @param BaseController $context
     */
    function task(BaseController $context)
    {
        $context->attach("nbArticlesInPanier", $this->getChildResult('panier')->countItem());
        $context->attach("panierCost", $this->getChildResult('panier')->getPrixTotal());
    }

}

/**
 * Classe SessionPanierTask
 *
 * Tâche qui récupère le panier à partir de la session et attache les données à smarty
 */
class SessionPanierTask extends Task {

    function task(BaseController $context)
    {
        $p = Panier::getPanier(false);

        $context->attach("panier", $p);

        return array("panier" => $p);
    }

}