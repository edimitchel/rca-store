{debug}
{extends file="struct/template.tpl"}
{block name="title"}Accueil{/block}
{block name="content"}
    <div class="row">
        <p class="lead welcome-message text-center">Bienvenue dans le monde du modélisme !</p>
        <div class="col-md-12">
            <h3>Articles les plus vendus</h3>

            <div class="row slider-best-article">
{if $bestarticles|@sizeof neq 0}
{foreach from=$bestarticles item=article}
                <div class="col-md-2 col-sm-6 col-xs-12">
                    <a href="/article/show/{{$article->getId()}}">
                        <img class="img-rounded image-article" src="{{imguri name=$article->getMiniature() dir="articles" }}" alt="" title="{$article->getLibelle()}" />
                    </a>
                    <button class="btn btn-success btn-xs ajout-panier" id="ajout_panier" data-id="{{$article->getId()}}"><span class="glyphicon glyphicon-plus"></span></button>
                </div>
{/foreach}
{/if}
            </div>
        </div>
        <div class="col-md-6">
            <h4>Derniers articles</h4>
            <ul>
                <li><a href="">lorem</a></li>
                <li><a href="">ipsum</a></li>
                <li><a href="">sdlmqsdk </a></li>
                <li><a href="">lorem dsqml</a></li>
                <li><a href="">sqdqs dlk</a></li>
            </ul>
        </div>
        <div class="col-md-6">
            <h4>Vidéo du mois</h3>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="//www.youtube.com/embed/4QgFVK-DKn4" frameborder="0" allowfullscreen></iframe>
            </div>
            <p class="text-center"><i>Flite Test - Sport Cub S</i></p>
        </div>
    </div>

{/block}
{block name="moreScript"}
    {importJs name="panier"}
    {importJs name="article"}
{/block}