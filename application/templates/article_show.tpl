{debug}
{extends file="struct/template.tpl"}
{block name="title"}{{$article->getLibelle()}}{/block}
{block name="content"}
    <div class="row">
        <div class="col-md-9">
            <h2>{{$article->getLibelle()}}</h2>

            <div class="row">
                <div class="col-md-5 col-sm-6">
                    <img class="img-rounded image-article" src="{{imguri name=$article->getMiniature() dir="articles" }}" alt="{{$article->getLibelle()}}">
                </div>
                <div class="col-md-7 col-sm-6">
                    <p>{{$article->getDescription()}}</p>
                    <div class="text-right">
                        <div class="prix">{{$article->getPrix()}}</div>
                        <button class="btn btn-success ajout-panier" id="ajout_panier" data-id="{{$article->getId()}}"><span class="glyphicon glyphicon-plus"></span> Ajouter au panier</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <h4>{{$sameArticles->count()}} article{if $sameArticles->count() >= 1}s{/if} similaires</h4>
            <div class="row no-gutter">
{foreach from=$sameArticles item=article}
                <a href="/article/show/{{$article->getId()}}">
                    <div class="col-sm-6 col-xs-3">
                        <img class="img-rounded image-article" src="{{imguri name=$article->getMiniature() dir="articles" }}" alt="{{$article->getLibelle()}}">
                    </div>
                </a>
{/foreach}
            </div>
        </div>
    </div>
{/block}
{block name="moreScript"}
    {importJs name="panier"}
    {importJs name="article"}
{/block}