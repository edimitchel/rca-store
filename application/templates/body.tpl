<div class="col-lg-9" id="wrapContent">
	<nav class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Basculer la navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">{$app_name}</a>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li{if $pageId eq "accueil"} class="active"{/if}><a href="/">Accueil</a></li>
				<li{if $pageId eq "catalogue"} class="active"{/if}><a href="/catalogue">Catalogue</a></li>
			</ul>
			<form class="navbar-form navbar-right" role="search" id="searchForm" method="GET" action="/catalogue/recherche/">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Recherche.."{if isset($query)} value="{$query}"{/if}>
				</div>
				<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
			</form>
{if !isset($isConnected) || $isConnected neq "true"}
			<ul class="nav navbar-nav navbar-right">
				<li{if $pageId eq "inscription"} class="active"{/if}><a href="/inscription">S'enregistrer</a></li>
			</ul>
{/if}
		</div>
	</nav>
	<div class="main-content">
		{block name="content"}{/block}
		<div style="clear: both"></div>
	</div>
</div>