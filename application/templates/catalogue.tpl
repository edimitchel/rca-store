{extends file="struct/template.tpl"}
{block name="title"}Catalogue{/block}
{block name="content"}
	<h3>Catalogue</h3>

{foreach from=$categories item=categorie}
		<div class="categories-list row no-gutter">
			<a href="/catalogue/categorie/{{$categorie->getId()}}">
				<h4>{{$categorie->getLibelle()}}</h4>
			</a>
			<div class="row">
{foreach from=$categorie->getArticles() item=article}
				<div class="article">
					{$article->getLibelle()}
				</div>
{/foreach}
			</div>
		</div>
{/foreach}

{/block}
