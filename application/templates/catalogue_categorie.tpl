{extends file="struct/template.tpl"}
{block name="title"}
    {$titre}
{/block}
{block name="content"}
	<h3>{$titreLink}</h3>

    <div class="row tile-gallery">
{foreach from=$articles item=article}
        <a href="/article/show/{{$article->getId()}}">
            <div class="col-sm-4 tile">
                <img class="img-rounded image-article" src="{{imguri name=$article->getMiniature() dir="articles" }}" alt="{{$article->getLibelle()}}"/>
                <div class="caption-content">
                    <div class="title">
                        {{$article->getLibelle()}}
                    </div>
                    <div class="price">
                        {{$article->getPrix()}}
                    </div>
                    <p class="description">{{$article->getDescription()|truncate:100}}</p>
                    <div class="btns">
                        <button class="btn btn-success ajout-panier" id="ajout_panier" data-id="{{$article->getId()}}"><span class="glyphicon glyphicon-plus"></span> Ajouter au panier</button>
                    </div>
                </div>
            </div>
        </a>
{/foreach}
    </div>

{/block}
{block name="moreScript"}
    {importJs name="panier"}
    {importJs name="article"}
{/block}

