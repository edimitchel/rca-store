{extends file="struct/template.tpl"}
{block name="title"}
    Recherche
{/block}
{block name="content"}
    <h4>Recherche pour <em>{$query}</em> : {$count} résultat{if $count > 1}s{/if}</h4>

{if $articles->count() neq 0}
    <div class="row tile-gallery">
{foreach from=$articles item=article}
            <a href="/article/show/{{$article->getId()}}">
                <div class="col-md-4 col-sm-6 tile">
                    <img class="img-rounded image-article" src="{{imguri name=$article->getMiniature() dir="articles" }}" alt="{{$article->getLibelle()}}"/>
                    <div class="caption-content">
                        <div class="title">
                            {{$article->getLibelle()}}
                        </div>
                        <div class="price">
                            {{$article->getPrix()}}
                        </div>
                        <p class="description">{{$article->getDescription()|truncate:100}}</p>
                        <div class="btns">
                            <button class="btn btn-success ajout-panier" id="ajout_panier" data-id="{{$article->getId()}}"><span class="glyphicon glyphicon-plus"></span> Ajouter au panier</button>
                        </div>
                    </div>
                </div>
            </a>
{/foreach}
{if isset($moreResult)}
            <div class="col-md-4 col-sm-6 tile pager">
                {if isset($prevPage)}<a href="{$prevPage}"><div class="text-center">précédent</div></a>{/if}
                <a href="{$nextPage}"><div class="text-center">suivant</div></a>
            </div>
        </a>
{/if}
    </div>
{else}
    <i>
        <h4>Aucun résultat pour la recherche "<strong>{$query}</strong>"</h4>
    </i>
{/if}
{/block}
{block name="moreScript"}
    {importJs name="panier"}
    {importJs name="article"}
{/block}
