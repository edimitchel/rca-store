{extends file="struct/template.tpl"}
{block name="title"}
	Compte - Mes commandes
{/block}
{block name="content"}

	<h3>Mes commandes</h3>
	<table class="table table-striped">
		<thead>
		<tr>
			<th width="60">#</th>
			<th>Contenu</th>
			<th>Date</th>
			<th>Statut</th>
		</tr>
		</thead>
		<tbody>
		{if $commandes|@sizeof eq 0}
			<tr><td colspan="4" class="text-center"><em>Aucune commande enregistrée.</em></td></tr>
		{else}
			{foreach from=$commandes item=commande}
				<tr>
					{if !$commande->isPaye()}
						<td><strong><a href="/reglement/finish/{$commande->getUserSavedId()}">{$commande->getUserSavedId()}</a></strong></td>
					{else}
						<td><strong>{$commande->getUserSavedId()}</strong></td>
					{/if}
					<td>
						<ul>
							{foreach from=$commande->getArticles() item=art}
								<li>{$art->getCount()} x <a href="/article/show/{$art->getId()}">{$art->getLibelle()}</a></li>
							{/foreach}
						</ul>
					</td>
					<td>{$commande->getDate()}</td>
					<td>{$commande->getEtat()}{if !$commande->isPaye()}<br><a href="/reglement/finish/{$commande->getUserSavedId()}"><button class="btn btn-success btn-sm">Payer</button></a>{/if}</td>
				</tr>
			{/foreach}
		{/if}
		</tbody>
	</table>
{/block}
{block name="moreScript"}
	{importJs name="panier"}
{/block}
