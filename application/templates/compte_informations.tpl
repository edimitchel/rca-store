{extends file="struct/template.tpl"}
{block name="title"}
	Compte - Mes informations
{/block}
{block name="content"}

	<h3>Mes informations</h3>
	<div class="row">
		<div class="col-md-6">
			<h4 class="lead">Informations générales</h4>

			<div class="entry-modification" data-name="nom">
				<label for="">Nom</label>
				<div class="content">
					<span>{$user->client->getNom()}</span>
					<i class="edit glyphicon glyphicon-pencil"></i>
					<i class="submit glyphicon glyphicon-floppy-disk"></i>
				</div>
			</div>
			<div class="entry-modification" data-name="prenom">
				<label for="">Prénom</label>
				<div class="content">
					<span>{$user->client->getPrenom()}</span>
					<i class="edit glyphicon glyphicon-pencil"></i>
					<i class="submit glyphicon glyphicon-floppy-disk"></i>
				</div>
			</div>
			<div class="entry-modification">
				<label for="">Adresse e-mail</label>
				<div class="content">
					<span>{$user->client->getMail()}</span>
				</div>
			</div>
			<div class="entry-modification" data-name="motdepasse" data-option="confirm cached password">
				<label for="">Mot de passe</label>
				<div class="content">
					<span>*********</span>
					<i class="edit glyphicon glyphicon-pencil"></i>
					<i class="submit glyphicon glyphicon-floppy-disk"></i>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<h4 class="lead">Adresse de livraison</h4>
			<div class="entry-modification" data-name="adresse">
				<label for="">Adresse</label>
				<div class="content">
					<span>{$user->client->getAdresse()}</span>
					<i class="edit glyphicon glyphicon-pencil"></i>
					<i class="submit glyphicon glyphicon-floppy-disk"></i>
				</div>
			</div>
			<div class="entry-modification" data-name="codepostal">
				<label for="">Code postal</label>
				<div class="content">
					<span>{$user->client->getCodepostal()}</span>
					<i class="edit glyphicon glyphicon-pencil"></i>
					<i class="submit glyphicon glyphicon-floppy-disk"></i>
				</div>
			</div>
			<div class="entry-modification" data-name="ville">
				<label for="">Ville</label>
				<div class="content">
					<span>{$user->client->getVille()}</span>
					<i class="edit glyphicon glyphicon-pencil"></i>
					<i class="submit glyphicon glyphicon-floppy-disk"></i>
				</div>
			</div>
		</div>
	</div>
{/block}
{block name="moreScript"}
	{importJs name="informations"}
{/block}
