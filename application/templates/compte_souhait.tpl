{extends file="struct/template.tpl"}
{block name="title"}
	Compte - Listes de souhaits
{/block}
{block name="content"}

	<h3>Liste de souhaits<span class="wrap" style="float:right">
			<button class="deletePanier btn btn-danger" title="Supprimer la liste de souhait"><span class="glyphicon glyphicon-trash"></span> Supprimer la liste</button>
</span></h3>
	<table class="table table-hover" id="panierTable" data-id="{$listesouhait->getUserSavedId()}">
		<thead>
			<tr>
				<th width="25">#</th>
				<th>Produit</th>
				<th class="text-center">Prix / unité</th>
				<th class="text-center">Nombre</th>
				<th class="text-center">Total</th>
			</tr>
		</thead>
		<tbody>
{if $listesouhait->countItem() eq 0}
	<tr><td colspan="5" class="text-center"><em>Aucun produit dans la liste de souhaits .. <a href="/ajax/supprimerlistesouhait/{$listesouhait->getId()}">supprimer la liste ?</a></em></td></tr>
{else}
	{foreach from=$listesouhait->getArticles() item=souhait}
		<tr>
			<td class="ref">{$souhait->getId()}</td>
			<td><a href="/article/show/{$souhait->getId()}">{$souhait->getLibelle()|truncate:40}</a></td>
			<td class="prix prixUnite text-center">{$souhait->getPrix()}</td>
			<td class="count text-center">{$souhait->getCount()}</td>
			<td class="total prix text-right"><strong>{$souhait->getPrixTotal()}</strong></td>
		</tr>
	{/foreach}
{/if}
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3"></td>
				<td><strong>TOTAL :</strong></td>
				<td class="text-right prix" id="prixTotal"><strong>{$listesouhait->getPrixTotal()}</strong></td>
			</tr>
		</tfoot>
	</table>

{if $listesouhait->countItem() neq 0}
	<p class="lead text-center">Votre liste de souhaits est prête à être commandée,<br>et vous, êtes-vous prêt{if isset($user)}{if $user->client->getCivilite() eq "f"}e{/if}{else}(e){/if} ?</p>
	<form action="/reglement" method="post" id="reglementForm" data-messageConfirm="Êtes-vous sûr{if isset($user)}{if $user->client->getCivilite() eq "f"}e{/if}{else}(e){/if}de vouloir procédé à la validation de la liste ?">
		<input name="typeCommande" type="hidden" value="souhait" />
		<input name="idSouhait" type="hidden" value="{$listesouhait->getUserSavedId()}" />
		<input name="origin" type="hidden" value="{$smarty.server.REQUEST_URI}" />
		<button class="btn btn-lg center-block">C'est parti !</button>
	</form>
{/if}
{/block}
{block name="moreScript"}
	{importJs name="panier"}
{/block}
