{extends file="struct/template.tpl"}
{block name="title"}
	Compte - Listes de souhaits
{/block}
{block name="content"}
	<h3>Mes listes de souhaits</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th width="60"># liste</th>
				<th>Contenu</th>
				<th>Prix</th>
				<th>Visualiser ?</th>
			</tr>
		</thead>
		<tbody>
{if $souhaits|@sizeof eq 0}
			<tr><td colspan="4" class="text-center"><em>Aucune liste à souhaits enregistrée.</em></td></tr>
{else}
{foreach from=$souhaits item=liste}
			<tr>
				<td><strong><a href="/compte/souhait/{$liste->getUserSavedId()}">{$liste->getUserSavedId()}</a></strong></td>
				<td>
					<ul>
{foreach from=$liste->getArticles() item=art}
						<li>{$art->getCount()} x <a href="/article/show/{$art->getId()}">{$art->getLibelle()}</a></li>
{/foreach}
					</ul>
				</td>
				<td class="prix">{$liste->getPrixTotal()}</td>
				<td><a href="/compte/souhait/{$liste->getUserSavedId()}"><button class="btn btn-success btn-sm">Visualiser &rightarrow;</button></a></td>
			</tr>
{/foreach}
{/if}
		</tbody>
	</table>
{/block}
{block name="moreScript"}
{/block}
