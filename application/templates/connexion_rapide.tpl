{if !isset($isConnected) || $isConnected neq "true"}
<div id="formConnexion">
	<h4 class="text-center">Connexion rapide</h4>
	<form action="/identification/connect" class="form" method="post">
		<div class="errors"></div>
		<input type="hidden" name="url" class="cntUrl" value="{$smarty.server.REQUEST_URI}" />
		<div class="form-group">
			<input type="email" name="mail" class="form-control cntMail" placeholder="Adresse e-mail">
		</div>
		<div class="form-group">
			<input type="password" name="mdp" class="form-control cntMdp" placeholder="Mot de passe">
		</div>
		<div class="form-group">
			<button class="btn btn-default">Connexion</button> <em>ou</em> <a href="/inscription">Inscrivez-vous ..</a>
		</div>
	</form>
</div>
{else}
	<h6 class="text-center">{$user->name}.</h6>
	<ul class="list-group">
		<li class="list-group-item">
			<a href="/compte/informations">Mes informations</a>
		</li>
		<li class="list-group-item">
			<a href="/compte/commandes">Mes commandes</a>
		</li>
		<li class="list-group-item">
			<a href="/compte/souhaits">Mes listes de souhaits</a>
		</li>
		<li class="list-group-item">
			<a href="/identification/disconnect">Déconnexion</a>
		</li>
	</ul>
{/if}