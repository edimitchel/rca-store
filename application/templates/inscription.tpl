{extends file="struct/template.tpl"}
{block name="title"}
	Inscription
{/block}
{block name="content"}
	<h1 class="text-center">Gestion du compte client</h1>
	<h3 class="text-center">Inscription</h3>
{include "inscription_formulaire.tpl"}
{/block}
{block name="moreScript"}
	{importJs name="inscription"}
{/block}
