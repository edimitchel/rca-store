<form action="/inscription/submit" method="post" class="form-horizontal col-md-10 col-md-offset-1" data-toggle="validator" role="form" id="formInscription">
{if isset($typeCommande)}
    <input name="typeCommande" type="hidden" value="{$typeCommande}" />
    <input name="origin" type="hidden" value="{$origin}" />
{/if}
{if isset($errors)}
        <div class="listing-errors text-center">
{foreach from=$errors item=e}
                <div class="text-danger">{$e}</div>
{/foreach}
        </div>
{/if}

    <div class="form-group">
        <label for="formClientCivilite" class="control-label col-sm-4">Civilité : </label>
        <div class="col-sm-8">
            <select class="form-control" name="civilite" id="formClientCivilite">
                <option value="h" {if isset($civilite) && $civilite eq "h"}selected="selected"{/if}>Monsieur</option>
                <option value="f" {if isset($civilite) && $civilite eq "f"}selected="selected"{/if}>Madame</option>
            </select>
        </div>
    </div>
    <div class="form-group" data-type="nom">
        <label class="control-label col-sm-4" for="formClientNom">Nom : </label>
        <div class="col-sm-8">
            <input type="text" name="nom" required="required" class="form-control" id="formClientNom" placeholder="ex: Dupond" pattern="[A-Za-z -]*" value="{$nom|default:""}">
        </div>
    </div>
    <div class="form-group" data-type="prenom">
        <label for="formClientPrenom" class="control-label col-sm-4">Prénom : </label>
        <div class="col-sm-8">
            <input type="text" name="prenom" required="required" class="form-control" id="formClientPrenom" placeholder="ex: Jean" value="{$prenom|default:""}">
        </div>
    </div>
    <div class="form-group" data-type="mail">
        <label for="formClientPrenom" class="control-label col-sm-4">Adresse e-mail : </label>
        <div class="col-sm-8">
            <input type="email" name="adressemail" required="required" class="form-control" id="formClientMail" placeholder="exemple@mail.com" value="{$adressemail|default:""}">
        </div>
    </div>
    <div class="form-group" data-type="motdepasse">
        <label for="formClientMotdepasse" class="col-sm-4 control-label">Mot de passe : </label>
        <div class="col-sm-8">
            <input type="password" name="motdepasse" required="required" class="form-control" pattern=".{ldelim}6,{rdelim}" id="formClientMotdepasse" placeholder="min. 6 caractères" value="{$motdepasse|default:""}">
        </div>
    </div>
    <div class="form-group" data-type="verifmotdepasse">
        <label for="formClientVerifMotdepasse" class="col-sm-4 control-label">Retapez le mot de passe : </label>
        <div class="col-sm-8">
            <input type="password" name="verifMdp" required="required" class="form-control" pattern=".{ldelim}6,{rdelim}" id="formClientMotdepasse" placeholder="le même mot de passe" value="{$verifmotdepasse|default:""}">
        </div>
    </div>
    <div class="form-group" data-type="adresse">
        <label for="formClientAdressePostale" class="col-sm-4 control-label">Adresse postale : </label>
        <div class="col-sm-8">
            <input type="text" name="adressepostale" required="required" class="form-control" id="formClientAdressePostale" placeholder="ex: 15, rue des belles fleurs " value="{$adressepostale|default:""}">
        </div>
    </div>
    <div class="form-group" data-type="codepostal">
        <label for="formClientCodePostal" class="col-sm-4 control-label">Code postal : </label>
        <div class="col-sm-8">
            <input type="text" name="codepostal" required="required" class="form-control" pattern="[0-9]*" id="formClientCodePostal" placeholder="ex: 67000" value="{$codepostal|default:""}">
        </div>
    </div>
    <div class="form-group" data-type="ville">
        <label for="formClientVille" class="col-sm-4 control-label">Ville : </label>
        <div class="col-sm-8">
            <input type="text" name="ville" required="required" class="form-control" id="formClientVille" placeholder="ex: Strasbourg" value="{$ville|default:""}">
        </div>
    </div>
    <div class="form-group form-right text-center">
        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Inscription</button>
        <button type="reset" class="btn btn-xs btn-default">Effacer</button>
    </div>
</form>