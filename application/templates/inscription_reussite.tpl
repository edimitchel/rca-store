{extends file="struct/template.tpl"}
{block name="title"}
	Inscription réussie !
{/block}
{block name="content"}
	<h2 class="lead text-success text-center">Bravo {$newuser->getPrenom()}, vous êtes désormais client{if $newuser->getCivilite() eq "f"}e{/if} chez RCA Store.</h2>
	<br/>
	<br/>

	<a href="/catalogue"><button class="btn btn-lg btn-success center-block">Consulter le catalogue maintenant</button></a>
{/block}
{block name="moreScript"}
	{importJs name="inscription"}
{/block}
