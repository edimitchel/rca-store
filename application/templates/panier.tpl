{extends file="struct/template.tpl"}
{block name="title"}
	Panier
{/block}
{block name="content"}
	<h3>Panier <span class="wrap" style="float:right">
{if $isConnected}
		<button class="savePanier btn btn-success" title="Sauvegarder en tant que liste à souhait"><span class="glyphicon glyphicon-save"></span> Enregistrer la liste à souhaits</button>
{/if}
		<button class="deletePanier btn btn-danger" title="Supprimer le panier"><span class="glyphicon glyphicon-trash"></span> Supprimer le panier</button>
</span></h3>

	<table class="table table-hover" id="panierTable">
		<thead>
			<tr>
				<th width="25">#</th>
				<th>Produit</th>
				<th width="150">Actions</th>
				<th class="text-center">Prix / unité</th>
				<th class="text-center">Nombre</th>
				<th class="text-center">Total</th>
			</tr>
		</thead>
		<tbody>
{if $panier->countItem() eq 0}
			<tr><td colspan="6" class="text-center"><em>Aucun produit dans le panier</em></td></tr>
{else}
{foreach from=$panier->getArticles() item=art}
			<tr>
				<td class="ref">{$art->getId()}</td>
				<td><a href="/article/show/{$art->getId()}">{$art->getLibelle()|truncate:40}</a></td>
				<td>
					<button class="delete btn btn-sm btn-danger" title="Supprimer"><span class="glyphicon glyphicon-trash"></span></button>
					<button class="add btn btn-sm btn-success" title="Augmenter"><span class="glyphicon glyphicon-plus"></span></button>
					<button class="remove btn btn-sm btn-danger" title="Diminuer"><span class="glyphicon glyphicon-minus"></span></button>
				</td>
				<td class="prix prixUnite text-center">{$art->getPrix()}</td>
				<td class="count text-center">{$art->getCount()}</td>
				<td class="total prix text-right"><strong>{$art->getPrixTotal()}</strong></td>
			</tr>
{/foreach}
{/if}
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4"></td>
				<td><strong>TOTAL :</strong></td>
				<td class="text-right prix" id="prixTotal"><strong>{$panier->getPrixTotal()}</strong></td>
			</tr>
		</tfoot>
	</table>

{if $panier->countItem() neq 0}
	<p class="lead text-center">Votre panier est prêt à être commandé, <br/>et vous, êtes-vous prêt{if isset($user)}{if $user->client->getCivilite() eq "f"}e{/if}{else}(e){/if} ?</p>
	<form action="/reglement" method="post" id="reglementForm" data-messageConfirm="Êtes-vous sûr{if isset($user)}{if $user->client->getCivilite() eq "f"}e{/if}{else}(e){/if} de vouloir procédé à la validation de la liste ?">
		<input name="typeCommande" type="hidden" value="panier" />
		<input name="origin" type="hidden" value="{$smarty.server.REQUEST_URI}" />
		<button class="btn btn-lg center-block">C'est parti !</button>
	</form>
{/if}

{/block}
{block name="moreScript"}
	{importJs name="panier"}
{/block}
