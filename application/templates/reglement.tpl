{debug}
{extends file="struct/template.tpl"}
{block name="title"}Règlement de votre commande{/block}
{block name="content"}
    <div class="row">
        <div class="col-md-12">
            <h3>Règlement de votre commande</h3>
            <table class="table table-hover" id="panierTable">
                <thead>
                    <tr>
                        <th width="25">#</th>
                        <th>Produit</th>
                        <th class="text-center">Prix / unité</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Total</th>
                    </tr>
                </thead>
                <tbody>
{if sizeof($commande->getArticles()) eq 0}
                    <tr><td colspan="6" class="text-center"><em>Aucun produit dans le panier</em></td></tr>
{else}
{foreach from=$articles item=art}
                        <tr>
                            <td class="ref">{$art->getId()}</td>
                            <td><a href="/article/show/{$art->getId()}">{$art->getLibelle()|truncate:40}</a></td>
                            <td class="prix prixUnite text-center">{$art->getPrix()}</td>
                            <td class="count text-center">{$art->getCount()}</td>
                            <td class="total prix text-right"><strong>{$art->getPrixTotal()}</strong></td>
                        </tr>
{/foreach}
{/if}
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3"></td>
                        <td class="text-right"><em>dont TVA (20%) :</em></td>
                        <td class="text-right prix"><strong>{$commande->getMontantTVA()}</strong></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td class="text-right" colspan="2"><strong>TOTAL à régler:</strong></td>
                        <td class="text-right prix" id="prixTotal"><strong>{$commande->getPrixTotal()}</strong></td>
                    </tr>
                </tfoot>
            </table>

            <h3 class="text-center">Paiement</h3>

            <div class="row">
                <div class="col-md-4"><p class="text-right">Le paiement est exclusivement réalisé par notre site internet. Vous n'avez aucune action a réaliser avec votre banque. Nous nous en chargeons. Nous respectons la norme 289273DIS (Norme obligatoire pour les paiement en ligne).</p></div>
                <div class="col-md-8">
                    <p><strong>L'adresse de livraison est:</strong></p>
                    <pre>{$user->client->getAdresse()}
{$user->client->getCodepostal()} {$user->client->getVille()}</pre>

                    <div class="text-callback lead text-center">La somme à régler s'élève à <strong>{$commande->getPrixTotal()} €</strong>.</div>
                    <form action="/paiement/index.php/{$user->client->getId()}/{$commande->getUserSavedId()}" method="POST" id="paiementForm">
                        <input id="idCommande" name="commandeid" type="hidden" value="{$commande->getUserSavedId()}" />
                        <input id="montant" name="montant" type="hidden" value="{$commande->getPrixTotal()}" />
                        <input id="devise" name="devise" type="hidden" value="euro" />
                        <button class="btn btn-default btn-lg center-block">Confirmer la commande.</button>
                    </form>
                </div>
            </div>


        </div>
    </div>

{/block}
{block name="moreScript"}
    {importJs name="reglement"}
{/block}