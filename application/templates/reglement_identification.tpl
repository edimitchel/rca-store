{debug}
{extends file="struct/template.tpl"}
{block name="title"}Identification avant règlement{/block}
{block name="content"}
    <div class="row">
        <div class="col-md-8">
            <p class="lead text-center">Pas encore inscrit ?</p>
            <a href="/inscription"><button class="btn btn-lg btn-success center-block">S'inscrire tout de suite</button></a>
        </div>
        <div class="col-md-4 connexion-rapide">
            <p class="lead text-center">Déjà inscrit(e) ?</p>
            {include "connexion_rapide.tpl"}
        </div>
    </div>

{/block}
{block name="moreScript"}
    {importJs name="paiement"}
{/block}