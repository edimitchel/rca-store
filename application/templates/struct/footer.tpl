				</div>
			</section>

		<footer class="text-center">
			<h5>RCA Store - Site e-commerce réalisé par Benjamin COULON et Michel EDIGHOFFER.</h5>
		</footer>

		</div>
		{block name="moreScript"}
			{importJs vendor=true name="components/jquery/jquery.min"}
			{importJs vendor=true name="bootflat/bootflat/js/bootstrap.min"}
			{importJs name="main"}
			{importJs name="connexion"}
			{$smarty.block.child}
		{/block}
	</body>
</html>