<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

{if isset($page_description)}
		<meta name="description" content="{$page_description}">
{/if}

		<title>{block name="title"}{$smarty.block.child} - {$app_name}{/block}</title>

{importCss vendor=true name="bootflat/bootflat/css/bootstrap.min"}
{importCss name="bootflat/bootflat.min"}
	</head>
	<body class="{$pageId}">
		<div class="container">
			<header id="header" class="row">
				<div class="ban col-lg-3">
					<div class="logo"></div>
				</div>
				<div class="ban col-lg-9">
					<h3 class="text-center">Le meilleur de l'aéromodélisme, dans votre panier!</h3>
				</div>
			</header>
			<section id="content">
				<div class="row">