<aside class="col-lg-3 nopadding">
	<div class="barlat">
		<div id="panierBtn" {if $pageId eq "panier"} class="active"{/if}>
			<a href="/panier">
				Panier <span class="glyphicon glyphicon-shopping-cart"></span>
{if isset($panierCost) && $pageId neq "panier" && $panierCost neq 0}
				<span class="nb">{$panierCost}</span>
{/if}
			</a>
		</div>
		<div class="categories">
			<h3 class="text-center">Catégories</h3>
			<ul class="list-group">
{foreach from=$categories item=cat}
				<li class="list-group-item{if isset($id_categorie) && $id_categorie eq $cat->getId()} listActive{/if}">
{if !(isset($id_categorie) && $cat->getId() eq $id_categorie && sizeof($child_categories) neq 0)}
        <a href="/catalogue/categorie/{$cat->getId()}">{$cat->getLibelle()}</a>
{else}
        <div class="list-group-item-heading">{$cat->getLibelle()}</div>
                    <ul>
{foreach from=$child_categories item=childcat}
                        <li class="list-group-item{if $id_child_categorie eq $childcat->getId()} listActive{/if}">
                            <a href="/catalogue/categorie/{$cat->getId()}/{$childcat->getId()}">{$childcat->getLibelle()}</a>
                        </li>

{/foreach}
                    </ul>
    </li>
{/if}
{/foreach}
			</ul>
		</div>
		{include file="connexion_rapide.tpl"}
	</div>
</aside>