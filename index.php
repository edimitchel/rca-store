<?php

	if(!defined("INITIALIZATION")) {

		define("INITIALIZATION", time());

		define("APP_DIR", "application");
		
		define("ASSETS_DIR", "assets");

		define("SYSTEM_DIR", "system");
		

		define("APP_PATH", __DIR__ . "/" . APP_DIR . "/");

		define("SYSTEM_PATH", __DIR__ . "/" . SYSTEM_DIR . "/");

		define("VENDOR_PATH", APP_DIR . "/" . "vendor" . "/");

		define("ASSETS_PATH", APP_DIR . "/" . ASSETS_DIR . "/");

	}

	require_once APP_PATH . "init.php";

	// Définition de la page NotFound
	FrontController::setNotFoundPage("404");

	// Ajout des tâches à exécuter
	ActionController::addTask(PanierTask::getTask());
	ActionController::addTask(CatalogueTask::getTask());
	ActionController::addTask(IdentificationTask::getTask());

	// Affiche le contrôleur suivant les paramètres dans le requête
	FrontController::dispatch();