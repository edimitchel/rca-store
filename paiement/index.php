<?php

    require '../application/vendor/slim/slim/Slim/Slim.php';

    \Slim\Slim::registerAutoloader();

    $app = new \Slim\Slim();

    //
    $app->post('/:idClient/:idCommande',
        function ($idClient, $idCommande) use ($app) {
            $request = $app->request;

            $montant = $request->post("montant");
            $devise = $request->post("devise");

            if($devise == 'euro')
                $devise = '€';
            elseif($devise == 'dollar')
                $devise = '$';
            elseif($devise == 'yen')
                $devise = 'sterling';

            sleep(2); // Simule un long traitement

            // Génére le code transaction
            $hash = sha1($idClient . $idCommande . time() . "rcaStore");
            $transactionId = substr($hash, rand(0, round(strlen($hash)/2)), round(strlen($hash)/2));

            header("Content-type: application/json");
            echo json_encode(array("status" => true, "data" => array(
                "transationID" => $transactionId,
                "msg" => "Le paiement n°<strong>" . $transactionId . "</strong> d'un montant de " . $montant . " " . $devise . " a bien été réalisé.")
            ));
            die;
        });
    $app->run();