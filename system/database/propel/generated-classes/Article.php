<?php

use Base\Article as BaseArticle;

/**
 * Skeleton subclass for representing a row from the 'article' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Article extends BaseArticle
{
    private $count = 1;

    public function getCount(){
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = $count;
    }


    public function getPrixTotal(){
        return $this->getPrix() * $this->getCount();
    }

    public function getPrix()
    {
        return round(parent::getPrix() + parent::getPrix() * (Panier::TVA / 100), 2);
    }


}
