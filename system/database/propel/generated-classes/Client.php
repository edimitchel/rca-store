<?php

use Base\Client as BaseClient;

/**
 * Skeleton subclass for representing a row from the 'client' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Client extends BaseClient
{
    public function setMotdepasse($v)
    {
        return parent::setMotdepasse(sha1($v . "rcastore"));
    }

    public function isMotdepasseSame($m)
    {
        return parent::getMotdepasse() === sha1($m . "rcastore");
    }


}
