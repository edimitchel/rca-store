-- phpMyAdmin SQL Dump
-- version 4.2.8
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 14 Janvier 2015 à 16:31
-- Version du serveur :  5.6.20
-- Version de PHP :  5.6.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `rcastore`
--

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`id`, `civilite`, `nom`, `prenom`, `adresse`, `codepostal`, `ville`, `motdepasse`, `mail`) VALUES
(1, 'h', 'COULON', 'Benjamin', 'azerty', '67000', 'azert', 'b73ee23afdc0f7cec7ad85c958768afb523fe07a', 'benj.c71@gmail.com');

--
-- Contenu de la table `commande`
--

INSERT INTO `commande` (`id`, `date`, `paye`, `id_client`) VALUES
(1, '2015-01-06 00:00:00', 1, 1),
(2, '0000-00-00 00:00:00', 0, 1),
(3, '0000-00-00 00:00:00', 0, 1),
(4, '2015-01-07 20:34:52', 1, 1),
(5, '2015-01-14 16:02:28', 1, 1),
(6, NULL, 0, 1);

--
-- Contenu de la table `commande_article`
--

INSERT INTO `commande_article` (`id_commande`, `id_article`, `quantite`) VALUES
(1, 4, 1),
(1, 7, 1),
(1, 6, 1),
(1, 14, 1),
(1, 15, 1),
(1, 16, 1),
(1, 8, 15),
(2, 14, 2),
(2, 36, 1),
(2, 63, 3),
(2, 64, 4),
(4, 14, 2),
(4, 22, 4),
(4, 36, 1),
(4, 63, 3),
(4, 64, 4),
(5, 6, 1),
(5, 8, 1),
(5, 37, 2),
(5, 48, 1),
(6, 24, 1),
(6, 57, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
