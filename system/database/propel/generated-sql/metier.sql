-- phpMyAdmin SQL Dump
-- version 4.2.8
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 14 Janvier 2015 à 16:28
-- Version du serveur :  5.6.20
-- Version de PHP :  5.6.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `rcastore`
--
--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id`, `libelle`, `parent`, `typeArticle`) VALUES
(4, 'Avions', NULL, NULL),
(5, 'Télécommandes', NULL, NULL),
(6, 'Accessoires', NULL, NULL),
(7, 'Carburants', NULL, NULL),
(8, 'Accus', 6, NULL),
(9, 'Thermiques', 4, NULL),
(10, 'Électriques', 4, NULL),
(11, 'À turbine', 4, NULL),
(12, 'Pour débutant', 5, NULL),
(13, 'Pour confirmé', 5, NULL),
(14, 'Méthanol', 7, NULL),
(15, 'Nitrométhane', 7, NULL),
(16, 'Huiles moteur', 7, NULL),
(17, 'Accessoires de terrain', 6, NULL),
(18, 'Accessoires pour planeurs', 6, NULL),
(19, 'Planeurs', 4, NULL),
(20, 'Accessoires autres', 6, NULL);


--
-- Contenu de la table `article`
--

INSERT INTO `article` (`id`, `libelle`, `description`, `prix`, `miniature`, `id_categorie`) VALUES
(1, 'Midget Mustang', 'Le Midget Mustang est une semi maquette de racer conçu en 1948 par Dave Long, ingénieur en chef de Piper Aircraft à l''époque. La première apparition publique de l''avion fut en à la courses National de Cleveland, ce n''était alors qu''un prototype.', 125, 'MidgetMustang_thermique.jpg', 9),
(3, 'Sky raider mach II', 'Le Sky Raider Mach II est la version aile basse du trainer Sky Raider Mach I. C''est un avion de transition qui permet de se perfectionner à la voltige après s''être initié au vol sur la version Trainer.', 90.93, 'Sky_raider_thermique.jpg', 9),
(4, 'Calmato Alpha 40 Sports Rouge (EP/GP)', 'D’une parfaite stabilité en vol, le Calmato Sport 40 Alpha de la marque de modélisme Kyosho est un modèle idéal pour bien débuter dans la catégorie aile basse. Les dernières étapes de montage, avant de pouvoir s’envoler, sont minimes et ne nécessitent aucun collage. La finition est irréprochable et ses performances sont surprenantes.', 134, 'Calmato_alpha_40_thermique.jpg', 9),
(5, 'Yak 54 3m10', 'Le YAK 54 est un avion de voltige Russe conçu dans les années 1990 capable d''emporter 2 passagers. ', 499, 'Yak_54_thermique.jpg', 9),
(6, 'Carbon-Z Cub BNF Basic', 'Voici le Carbon-Z Cub de la marque de modélisme E-Flite, la version body buildé de l’UMX carbon Cub. Il pourrait même être qualifié de « petit gros » tant son envergure de 2.15m est impressionnante, c’est le modèle le plus grande de la gamme utilisant la conception Carbon-Z.', 439, 'Carbon-Z_thermique.jpg', 9),
(7, 'Piper J3 PNP', 'Déjà bien connu du grand public, Famous model nous propose sa version semi-maquette du Piper J3.', 120, 'Piper_j3_PNP_electrique.jpg', 10),
(8, 'Apprentice S RTF mode 1', 'Apprentice S avec technologie SAFE qui donne une nouvelle dimension aux technologies de stabilisation. Le système SAFE est conçu pour limiter l''enveloppe de vol de l''avion apprentice et ainsi éviter les pertes de contrôle liées à des angles trop importants. ', 299, 'Apprentice_electrique.jpg', 10),
(9, 'Mini extra 300 3G3X', ' Le mini extra 300 3G3X fait parti des nouveaux produits développés par Skyartec; Ce mini avion peut évoluer dans un espéce très restreint. Ce mini avion vous épatera par sa stabilté alliée à une agilité remarquable, notamment grâce à la technologie 3G3X développée par Skyartec. Le vol est très fluide même lorsqu''il y a du vent. De plus le modèle est doté d''une motorisation brushless qui vous permettra de vous éclater en réalisant une multitude de figures accrobatiques. ', 100, '3G3X_electrique.jpg', 10),
(10, 'Piper Archer RTF mode 2', 'Voici la dernière nouveauté PARZONE, le PIPER ARCHER, fidèle reproduction des dernières versions de ce célèbre avion civil, ce modèle de 935mm d’envergure comblera tous les pilotes souhaitant une maquette facile à faire voler et facile à transporter. De plus, sa puissante motorisation brushless lui permet d’effectuer des figures acrobatiques', 119, 'Piper_Archer_electrique.jpg', 10),
(11, 'UMX Pitts S-1S BNF Basic', ' Découvrez le dernier modèle dans la gamme des UMX: le Pitts S-1S. Il est, comme les derniers UMX, doté de la technologie AS3X, ce qui le rend très facile à piloter et à obtenir de la stabilité ou de l''agilité lorsqu''on le souhaite. On peut le faire voler dans des espaces très restreints. Il en résulte des qualités de vol équivalentes à celles d''un avion de voltige de grande échelle qui aurait été réglé par un expert.', 129, 'EFLU5250-UMX-pitts-eflite_electrique.jpg', 10),
(12, 'AXION - Skywalker Rtf 2.4G Brushed Mode 1', 'Le Skywalker est un avion électrique 4 voies idéal pour se familiariser avec le pilotage à un budget très raisonnable.\r\nCette version est équipé d''un moteur à charbons et du système de transmission 2.4G afin de sécuriser vos vols.', 107, 'turbine_1.jpg', 11),
(13, 'AXION - At-6 Texan Rtf 2.4Ghz', 'Avion radiocommandé At-6 Texan, vendu en version Rtf (complet, prêt à voler), avec un ensemble radiocommande en 2.4Ghz.\r\nCet avion rc est de la marque modélisme Axion Rc.\r\nAvion radiocommandé Semi maquette de l'' At-6 Texan.', 83.42, 'turbine2.jpg', 11),
(14, 'HYPE', 'Caractéristiques générales :\r\nReproduction fidèle à l''originale\r\nQualité à basse vitesse\r\nVolets d''atterrissage\r\nLed intégrées\r\nKit complet prêt à voler\r\n4 mini servos installés', 162, 'turbine3.jpg', 11),
(15, 'AXION - I-15 Polikarpov Artf', 'Avion radiocommandé I-15 Polikarpov, vendu en version Artf de la marque modélisme Axion Rc.\r\nLe Polikarpov I-15 est un avion radiocommandé biplan électrique 2 axes.', 47.95, 'turbine4.jpg', 11),
(16, 'DYNAM - GeeBee Y Rtf mode 1', 'GeeBee Y, avion télécommandé électrique Rtf(prêt à voler) avec une télécommande en mode 1(gaz à droite) de marque de modelisme Dynam Belle reproduction volante du célèbre Geebee Y de 1931, cet avion Dynam vol également très bien.', 191.9, 'turbine5.jpg', 11),
(17, 'ELEKTRO-ROOKIE S RTF', 'Avion radiocommandé Elektro-Rookie S RTF : Ce kit est livré complet avec la radiocommande MX-10 Hott. Cette radiocommande programmable vous sera utile pour évoluer en aéromodélisme.Descriptions Elektro-Rookie :- Ensemble ELEKTRO-ROOKIE S avec radiocommande MX-10 Graupner HoTT- Modèle d''avion radiocommandé terminé, robuste, avec éléments en mousse Graupner SOLIDPOR®', 288, 'planneur1.jpg', 19),
(18, 'FREEMAN 1600 RTF', 'Avion radiocommandé Freeman 1600, vendu en version RTF (complet, prêt à voler) avec un ensemble radiocommande en 2.4Ghz. Cet avion rc, est de la marque modélisme Josway. L''avion radiocommandé Freeman 1600 est l''avion rc de début de la marque modélisme JOYSWAY.', 157.4, 'planneur2.jpg', 19),
(19, 'UMX RADIAN BNF - E-FLITE', 'UMX Radian BNF - E-flite Petit modèle très sympa à prendre en main, ce motoplaneur micro dispose du système de stabilisation AS3X. Il dispose de 3 voies : gaz, profondeur et dérives. Les ailes amovibles permettent le transport aisé du modèle.', 99, 'planneur3.jpg', 19),
(20, 'WP V-VENTURE ELEKTRO RTF FLUGMODELL - GRAUPNER', 'WP V-VENTURE Elektro RTF Flugmodell - Graupner Le V-VENTURE est un excellent modèle de début dont les qualités de vol ne laissent personne insensible. D''entrée de jeu, le modéliste apprend le pilotage 3 axes, aux ailerons. Dans toutes les situations, ce modèle particulièrement attrayant, réagit aux ordres de commande du pilote.', 299, 'planneur4.jpg', 19),
(21, 'E-EYES RTF - WALKERA', 'E-eyes, avion planeur radio-commandé livré prêt à voler (RTF) avec une télécommande devo F7 de marque de modelisme Walkera Le E-eyes de Walkera est un nouveau planeur, ou moto-planeur RC pour débutant. Cet avion télécommandé est idéal pour débuter en aéromodelisme.', 448, 'planneur5.jpg', 19),
(22, 'BLH2028-TÉLÉCOMMANDE', 'BLH2028-Télécommande LP6DSM Mode 2 avec fonction SAFE 200 SR X - Blade ', 89.9, 'debutant1.jpg', 12),
(23, 'DX2E SR201 - SPEKTRUM', 'DX2E SR201 - Spektrum  Nouvelle version avec antenne protégée dans une coque rigide La DX2E est la radiocommande 2 voies qui rend la technologie 2.4GHz Spektrum accessible à tous, malgré son prix mini, elle vous offre de multiples réglages et un magnifique boitier noir mat qui résiste aux projections de carburant. ', 59, 'debutant2.jpg', NULL),
(24, 'DX5E DSMX 2.4GHZ M2 AVEC RECEPTEURAR600 - SPEKTRUM', 'DX5e DSMX 2.4Ghz M2 avec recepteurAR600 - Spektrum Spektrum révolutionne une fois de plus le monde de la Radiocommande RC, avec ce modèle 2.4 Ghz à prix imbattable. La DX5e dispose d''une grande précision de contrôle grace à la technologie DSMX. Son design est sobre et efficace, ce qui la rend encore plus facile à utiliser.', 89, 'debutant3.jpg', 12),
(25, 'TÉLÉCOMMANDE 6 VOIES 2.4GHZ UPGRADED - DJI', 'Télécommande 6 voies 2.4Ghz UPGRADED - DJI - Indicateur de niveau de batterie - Accu lipo 2200mAh - Nouvelle Molette de contrôle nacelle', 99, 'debutant4.jpg', 12),
(26, 'H107-16-Télécommande  HUBSAN X4 ', 'H107-16-Télécommande  HUBSAN X4 ', 29, 'debutant5.jpg', 12),
(27, 'DX18 DSMX 18 VOIES (TX/RX) AVEC AR9020 MODE 2', 'DX18 DSMX 18 voies (Tx/Rx) avec récepteur AR9020. Ensemble radio-commande livré en Mode 2 (Gazà gauche). Une radio, de la marque modélisme Spektrum. La DX18 Spektrum est une radio haute de gamme pour les pilotes expérimentés. Grâce à ses programmes évolués pour avion, hélicoptères et planeur elle s''adapte à tous les pilotes.', 598, 'confirme1.jpg', 13),
(28, 'DX18QQ AR12120 - SPEKTRUM', 'Radio-commande DX18QQ livrée avec son récepteur AR12120, de la marque modélisme Spektrum. Cette radio est une édition limitée à 400 exemplaires pour l''europe.Pionnier dans le domaine de la voltige 3D et dessinant les avions radiocommandés de demain, Quique Somenzini est l''un des pilotes radiocommandés les plus influents du monde des modèles réduits.', 959, 'confirme2.jpg', 13),
(29, 'DX8 SPEKTRUM2.4GHZ ', 'Radio-commande DX8 2.4GHz, livré avec son récepteur AR8000. Une radiocommande, de la marque modélisme Spektrum. L''équipe Spektrum a encore frappé très fort. Cette fois, c''est avec la nouvelle radio DX8. Télémétrie en temps réel, grand écran rétro-éclairé, 30 modèles en mémoire, journaux des vols, temps de réponse augmenté.', 359, 'confirme3.jpg', 13),
(30, 'DEVO F7 RTF M1 - WALKERA', 'Devo F7 RTF M1 de marque de modelisme Walkera La Devo 7F est la toute dernière radio Walkera pour le vol en immersion (FPV). Elle intègre un écran lcd tactile de 3,5 pouces. Cette radio 7 voies vous permet de préparer très rapidement votre modèle télécommandé pour le vol en immersion sur votre hélico, avion, drone, quadri, voiture, bateau, sous-marin....les possibilités sont très nombreuse.', 299, 'confirme4.jpg', 13),
(31, 'AURORA 9 - OPTIMA 9 EN 2.4GHZ', 'Radio aurora 9 vendu avec son récepteur Optima 9 de marque Hitec AURORA 9 AFHSS 2,4GHz L'' AURORA 9 Hitec est une vraie révolution en matière de contrôle d''un aéromodèle. Une nouvelle ère commence avec un système exclusif de transmission en 2,4GHz AFHSS avec scanning intelligent des fréquences disponibles. ', 328, 'confirme5.jpg', 13),
(32, 'Mallette alu pour outillage/radio', 'Mallette alu pour outillage/radio format 43 x 21 x 30 cm - JP-5508878 Superbe malle de transport tout en aluminium de  qualité pour votre materiel de terrain : Dimensions intérieures - 425mm (L) x 217mm (l) x 297mm (H) ', 35.9, 'terrain1.jpg', 17),
(33, 'Set de caisse à outils 3 en 1', 'Set de caisse à outils 3 en 1  Set de box extrêmement robustes et pratique, composé de 3 dimensions différentes - gros, moyen, petit, la 4ème pièce étant le panier dans le plus petit box.', 44.9, 'terrain2.jpg', 17),
(34, 'Mallette aluminium outillage A2PRO', 'Mallette aluminium outillage A2PRO Dimensions extérieur : 455 x 260 x 305 mm Dimensions intérieur : 440 x 240 x 250 mm', 47.9, 'terrain3.jpg', 17),
(35, 'Standbox Tapis de travail (Kneeling Cushion)', 'Tapis de travail Standbox. Très pratique, protège vos genoux.\r\nDimensions : 475mm x 225mm x 35mm\r\nCouleur : Jaune\r\nMousse dense waterproof.', 3.96, 'terrain4.jpg', 17),
(36, 'Align Démarreur Haut de gamme', 'Démarreur Haut de gamme STQ 100 Avion 7 a 50cc - Align - HFSSTQ02T Caractéristiques Démarreur Haut de gamme STQ 100 Avion - Align - HFSSTQ02T : - Design révolutionnaire et ergonomique. - Indicateur de Tension de la batterie - Très bonne prise en main grace au grip', 94.9, 'terrain5.jpg', 17),
(37, 'Kit de commande par câble (2m)', 'Kit de commande complet par câble comprenant :\r\n2 mètres de câble tressé Ø 0,5mm ,\r\n2 chapes nylon à axe métal,\r\n2 embouts tendeurs,\r\n2 serres-câbles', 2.9, 'accessoirePlaneur1.jpg', 18),
(38, 'Kit de commande par câble (5m)', 'Kit de commande complet par câble comprenant :\r\n5 mètres de câble tressé gainé Ø 0,7mm ,\r\n4 chapes nylon à axe métal,\r\n4 embouts tendeurs,\r\n4 serres-câbles', 5.8, 'accessoirePlaneur2.jpg', 18),
(39, 'Clé d''aile carbone 16 mm', 'Diamètre : 16mm\r\nLongueur : 1 mètre (*)\r\nPoids mètrique : 294 g/mètre.\r\nConstruction : Fibre unidirectionnelle - résine époxy.', 29.1, 'accessoirePlaneur3.jpg', 18),
(40, 'Vis nylon 2x25mm (x10)', 'Vis nylon 2x25mm\r\n10 pièces', 2.5, 'accessoirePlaneur4.jpg', 18),
(41, 'Velcro autocollant noir 25mm x 20cm', 'Velcro autocollant noir 25mm x 20cm', 1.1, 'accessoirePlaneur5.jpg', 18),
(42, 'LiIo-Nano. Akku A123 1/1100 3,3V Einzelz', 'Caractéristiques\r\nDimensions : D18 x 66,5mm\r\nVoltage : 3.3 V\r\nCapacité : 1100 mah\r\nDécharge 30C en continu 60C en pointe\r\nPoids: 40g', 11.95, 'accu1.jpg', 8),
(43, 'Accu 1S 300mAh 35C CBL - Flytown', ' Accu 1S 300mAh 35C CBL - Flytown', 4.9, 'accu2.jpg', 8),
(44, 'Dynamite Reaction Air 3.7V 250mAh 1S 20C LiPo', 'Type: Batterie Lithium Polymer\r\nCapacité: 250mAh\r\nVoltage: 3.7V\r\nNombre de cellules: 1\r\nMax déchargement continu: 20C?', 4.9, 'accu3.jpg', 8),
(45, 'Accu Li-Po MCPX 3.7V 300mah 1S 35C', 'Accu Li-Po MCPX 3.7V 300mah 1S 35C\r\n\r\nType : LiPo\r\nCapacité : 300mAh\r\nTension : 3.7v\r\nDécharge en continu: 35C\r\nDimensions : 41 x 17 x 6\r\nTyoe de prises : Micro E-Flite\r\nFabricant: Flytown', 4.9, 'accu4.jpg', 8),
(46, 'Batterie Lipo 3.7V 1S 70mA 14C E-FLITE', 'Caractéristiques techniques\r\nType de la batterie	LiPO\r\nCapacité batterie	70 (mAh)\r\nNombre d''éléments	1 (S)\r\nVoltage	3.7 (V)\r\nCourant de décharge en continu	1 (C)\r\nCourant de décharge max	15 (C)\r\nPoids	5.1 (g)\r\nLongueur	2.55 (mm)\r\nLargeur	33 (mm)\r\nHauteur	11.7 (mm)', 5.1, 'accu5.jpg', 8),
(48, 'HÉLICE GWS DIRECT DRIVE', 'Caractéristiques techniques\r\nREF :	26025/008\r\nDimension :	2.5" x 0.8"\r\nAlésage :	1.0 mm à 2.0 mm\r\nUtilisation :	électrique\r\nMatière :	Nylon\r\nPropulsive :	Non', 2.5, 'autre1.jpg', 20),
(49, 'HÉLICE APC THIN-ELECTRIC 4.1" X 4.1"', 'Les hélices APC sont reconnues dans le monde entier pour leurs qualités.\r\nTous les profils sont optimisés par ordinateur afin d''obtenir un rendement maximum tout en réduisant leur niveau sonore.\r\nElles sont injectées en nylon chargé de fibre pour être légères tout en gardant une parfaite rigidité.', 5, 'autre2.jpg', 20),
(50, 'CHARGEUR ULTRAMAT 8', 'Chargeur à coupure automatique avec équilibreur intégré, pouvant être alimenté en 12V ou en 220 V au choix', 59.95, 'autre3.jpg', 20),
(51, 'TRAIN PRINCIPAL ACIER - 375 MM', 'En acier. 375 x 75 mm', 12, 'autre4.jpg', 20),
(52, 'JAMBE DE TRAIN AVANT 3 X 140', 'Jambe à couder avec une queue de cochon, très bon amorti. \r\nDiamètre:3mm. \r\nH:140mm. \r\nh:100 mm.', 4.3, 'autre5.jpg', 20),
(53, 'CARBURANT MODEL TECHNICS - DYNAGLO 5', 'Carburant universel adapté à tous les types de moteur.\r\nCaractéristiques techniques\r\nRef :  1140510\r\nContenance :	1.00 L\r\nNitro :	5 %', 10.9, 'nitro1.jpg', 15),
(54, 'CARBURANT MODEL TECHNICS - TECHPOWER', 'Carburant haute performance d''usage universel à base d''huile de synthèse nouvelle génération "EDL2", du nouveau pack d''additifs SICAL et de méthanol.', 20.85, 'nitro2.jpg', 15),
(55, 'CARBURANT MODEL TECHNICS - DYNAGLO 5 - 5 % 2.50 L', 'Carburant universelle adapté à tous type de moteur.', 18.9, 'nitro3.jpg', 15),
(56, 'CARBURANT MODEL TECHNICS - SUPAGLO 10 - 10 % 5.00 L', 'arburant spéciale pour les grosses cylindrées, et les moteurs à forte puissance: turbine, hors-bords, hélicoptére ou encore les moteurs fonctionnant à plein régime et mal refroidi.', 39.8, 'nitro4.jpg', 15),
(57, 'CARBURANT MECCAMO - STRATOS 4 STOKE - 15 % 5.00 L', 'Idéal en avion avec moteur 4 temps\r\n\r\nIdentification :Stick rond jaune\r\nAspect :Liquide jaune fluo translucide\r\nConditionnement :Emballages plastiques opaques barrière anti U.V.', 32.29, 'nitro5.jpg', 15),
(58, 'HUILE MVX 1000 2T - YACCO', 'Huile MVX 100O 2T pour moteur essence.\r\nIl s''agit d''une huile est 100% synthése formulé spécialement pour les moteurs refroidis par air ou par eau.', 15, 'huile1.jpg', 16),
(59, 'COLORANT ROUGE POUR CARBURANT', 'Colorant rouge permettant de teinter le carburant.\r\nCe produit ne modifie absolument pas les qualités du carburant et est complétement neutre vis à vis du moteur.', 5.5, 'huile2.jpg', 16),
(60, 'AFTER RUN MODEL TECHNICS - MODEL TECHNICS', 'Offre une protection supplémentaire contre la corrosion.\r\nIl suffit d''en mettre quelques gouttes par la bougie et de brasser l''hélice.\r\nCette huile peut également être utilisé lors du remontage d''un moteur afin de pré-lubrifier les piéces en mouvement.', 7.8, 'huile3.jpg', 16),
(61, 'Huile Micromotul (Bidon 2L)', 'Reconnue pour ses qualités par tous les modélistes, cette huile est un très grand classique. Ses qualités de lubrification en font une valeur sûre, pour tous les modélistes qui mixent eux-mêmes leurs composants. Idéal pour tous types de modélismes, que ce soit le modélisme avion, voiture/auto, mais aussi le modélisme hélico, sans oublier le RC bateau.', 20.95, 'huile4.jpg', 16),
(62, 'Huile Klotz KL-198', '\r\n\r\nElaboré sur la base de la Klotz TechniPlate, la formule KL 198 est très fluide et légère pour un lubrifiant 100% synthétique pur. \r\n\r\n- Améliore la réponse de l''accélérateur et offre une fluidité accrue,\r\n- Des ajustements d''aiguilles plus facile,\r\n- Plus douce transition de petite à grande vitesse.\r\n- KL-198 augmente l''économie de carburant, moins de traînée\r\n- Plus de puissance.', 28, 'huile5.jpg', 16),
(63, 'CARBURANT MODEL TECHNICS - DYNAGLO 5', 'Carburant universel adapté à tous les types de moteur.\r\nCaractéristiques techniques\r\nRef :  1140510\r\nContenance :	1.00 L\r\nNitro :	5 %', 10.9, 'nitro1.jpg', 14),
(64, 'CARBURANT MODEL TECHNICS - TECHPOWER', 'Carburant haute performance d''usage universel à base d''huile de synthèse nouvelle génération "EDL2", du nouveau pack d''additifs SICAL et de méthanol.', 20.85, 'nitro2.jpg', 14),
(65, 'CARBURANT MODEL TECHNICS - DYNAGLO 5 - 5 % 2.50 L', 'Carburant universelle adapté à tous type de moteur.', 18.9, 'nitro3.jpg', 14),
(66, 'CARBURANT MODEL TECHNICS - SUPAGLO 10 - 10 % 5.00 L', 'arburant spéciale pour les grosses cylindrées, et les moteurs à forte puissance: turbine, hors-bords, hélicoptére ou encore les moteurs fonctionnant à plein régime et mal refroidi.', 39.8, 'nitro4.jpg', 14),
(67, 'CARBURANT MECCAMO - STRATOS 4 STOKE - 15 % 5.00 L', 'Idéal en avion avec moteur 4 temps\r\n\r\nIdentification :Stick rond jaune\r\nAspect :Liquide jaune fluo translucide\r\nConditionnement :Emballages plastiques opaques barrière anti U.V.', 32.29, 'nitro5.jpg', 14);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;