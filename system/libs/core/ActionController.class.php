<?php

/**
* 
*/
class ActionController
{

	protected $request;

	protected $response;

	/**
	 * @param Request $request
	 * @param Response $response
	 */
	public function __construct(Request $request, Response $response){
		$this->request = $request;
		$this->response = $response;
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @return bool
	 */
	public static final function process(Request $request, Response $response)
	{
		$ext = ".class.php";
		$path = APP_PATH . "controllers/";
		$class = ucfirst($request->getModule()) ."Controller";
		$file = $path . $class . $ext;
		
		if (!file_exists($file) && !class_exists($class)){
			// Si le controlleur n'existe pas, on affiche le controlleur permettant d'afficher une page 404.
			$request->setNoFoundRequest();
			$class = ucfirst($request->getModule()) ."Controller";
			$file = $path . $class . $ext;
		}

		require_once $file;

		$controller = class_exists($class) ? new $class($request, $response) : false;
		
		if($controller === false)
			return false;
		
		$controller->launch();

		return $controller;
	}

	/**
	 * @param Task $t
	 */
	public static final function addTask(Task $t){
		array_push(BaseController::$tasks, $t);
	}

	/**
	 * @param bool $i
	 * @return array|bool
	 */
	public final function getDatas($i = false)
	{
		return $this->request->getData($i);
	}

	/**
	 * @return Response
	 */
	protected final function getResponse(){
		return $this->response;
	}
}

?>