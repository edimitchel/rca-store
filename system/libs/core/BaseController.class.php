<?php
/**
* 
*/
abstract class BaseController extends ActionController
{	
	protected $smarty;

	protected $isDataController = false;

	protected $isIndex = false;

	private $isDisplay = false;

	const CONTROLLER_PREFIX = "_";

	protected static $tasks = array();

	protected function configure() {

	}

	/**
	 * Action par défaut d'un contrôleur
	 * @return mixed
	 */
	public abstract function index();

	/**
	 * @return mixed
	 */
	public final function launch(){
		$this->configure();

		$this->smarty = RCASmarty::getInstance();
		$this->smarty->assign('pageId', $this->request->getModule());

		$this->runTasks();

		$action = $this->request->getAction();

		if($action !== false && method_exists($this, self::CONTROLLER_PREFIX . $action)){
			return $this->$action();
		} else {
			$this->isIndex = true;
			return $this->index();
		}
	}

	/**
	 * Execute les tâches
	 */
	private final function runTasks(){
		foreach(self::$tasks as $t){
			$t->run($this);
		}
	}

	/**
	 * @param $method
	 * @param $arguments
	 * @return bool
	 */
	public final function __call($method, $arguments){
		if($method != "index" && $method != "display" && $method != "launch" && $method != "setHaveNoIhm" && $method != "configure") {
			$a = self::CONTROLLER_PREFIX . $method;
			call_user_func_array(array($this,$a), $this->getDatas());
		} else {
			return false;
		}
	}

	/**
	 * @param $key
	 * @param $value
	 */
	public final function attach($key, $value){
		$this->smarty->assign($key, $value);
	}

	/**
	 * @param boolean $isDataController
	 */
	public function setIsDataController($isDataController)
	{
		$this->isDataController = $isDataController;
	}

	/**
	 * Affiche le template
	 * @param string $page
	 */
	public final function display($page = false){
		if(!$this->isDisplay) {
			if(!$this->isDataController) {
				if ($page === false) {
					if (!$this->isIndex && $this->smarty->templateExists($this->request->getModule() . '_' . $this->request->getAction() . '.tpl'))
						$this->smarty->display($this->request->getModule() . '_' . $this->request->getAction() . '.tpl');
					else if ($this->isIndex)
						$this->smarty->display($this->request->getModule() . '.tpl');
				} else {
					$this->smarty->display($page . ".tpl");
				}
				$this->isDisplay = true;
			} else {
				header("Content-typ: application/json");
				echo $this->response->toString();
			}
		}
	}
}
?>