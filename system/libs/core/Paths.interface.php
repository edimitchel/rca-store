<?php

define('TEMPLATE', APP_PATH . "templates");
define('TPL_COMPILE',  SYSTEM_PATH . "templates_c");
define('TPL_CONFIG', "configs");
define('TPL_CACHE',  APP_PATH . "cache");


interface Paths {
    const APP = APP_PATH;
    const SYSTEM = SYSTEM_PATH;
    const VENDOR = VENDOR_PATH;
    const ASSETS = ASSETS_PATH;

    const TEMPLATE = TEMPLATE;
    const TPL_COMPILE = TPL_COMPILE;
    const TPL_CONFIG = TPL_CONFIG;
    const TPL_CACHE = TPL_CACHE;
}