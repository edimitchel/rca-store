<?php

/**
* 
*/
class Request
{

	const INDEX_REQUEST = "accueil";

	private static $NOTFOUND_REQUEST = "404notfound";

	private $module;
	private $action;
	private $data = array();

	/**
	 * @return Request
	 */
	public static function getByRoute()
	{
		$r = new Request();
		$r->route();
		return $r;
	}

	/**
	 * @param $pagename
	 */
	public static function setNotFoundPage($pagename){
		self::$NOTFOUND_REQUEST = $pagename;
	}

	function setIndexRequest(){
		$this->module = self::INDEX_REQUEST;
	}

	function setNoFoundRequest(){
		$this->module = self::$NOTFOUND_REQUEST;
	}

	public function route(){
		$this->module = self::get('module');
		if($this->module == false || empty($this->module))
			$this->setIndexRequest();
		$this->action = self::get('action');

		$data = self::get('data');
		$this->data = explode("/", $data);
	}

	public function getModule(){
		return $this->module;
	}

	public function getAction(){
		return $this->action;
	}

	/**
	 * @param bool $i
	 * @return array|bool
	 */
	public function getData($i = false){
		if($i < 0 || $i > sizeof($this->data)-1)
			return false;
		return $i === false ? $this->data : $this->data[$i];
	}

	/**
	 * @param $key
	 * @return string
	 */
	public final static function get($key){
		return isset($_GET[$key]) ? $_GET[$key] : false;
	}

	/**
	 * @param $key
	 * @return string
	 */
	public final static function post($key){
		return isset($_POST[$key]) ? $_POST[$key] : false;
	}
}

?>