<?php

abstract class Task {
    protected $childs = array();

    protected $result = array();

    private function __construct(){}

    protected function preTask(){
    }

    /**
     * @param BaseController $context
     * @return mixed
     */
    abstract function task(BaseController $context);

    /**
     * @param BaseController $context
     * @return mixed
     */
    public final function run(BaseController $context){
        $this->preTask();
        $this->executeChild($context);
        return $this->task($context);
    }

    /**
     * @param $context
     */
    protected final function executeChild($context){
        if(!empty($this->childs)) {
            foreach($this->childs AS $c) {
                $r = $c->run($context);
                if($r != null){
                    if(is_array($r)){
                        $this->result = array_merge($this->result, $r);
                    } else {
                        $this->child_result[get_class($c)] = $c->run($context);
                    }
                }
            }
        }
    }

    /**
     * @param Task $child
     */
    protected final function addChild(Task $child){
        array_push($this->childs, $child);
    }

    /**
     * @param string $key
     * @return array|bool
     */
    public function getChildResult($key = false){
        if($key === false)
            return $this->result;
        elseif(!isset($this->result[$key])){
            return false;
        } else {
            return $this->result[$key];
        }
    }

    /**
     * @return mixed
     */
    public static final function getTask(){
        $class = get_called_class();
        return new $class();
    }
}