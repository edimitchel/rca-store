<?php

	/**
	* 
	*/
	class RCASmarty
	{

		private static $_instance = false;

		private function __construct() {}
		
		public static function getInstance() {
			if(self::$_instance === false) {
				include_once "helpers.smarty.php";

				$smarty = new Smarty();
				$smarty->setTemplateDir(Paths::TEMPLATE);
				$smarty->setCompileDir(Paths::TPL_COMPILE);
				$smarty->setConfigDir(Paths::TPL_CONFIG);
				$smarty->setCacheDir(Paths::TPL_CACHE);

				$smarty->caching = false;
				$smarty->force_compile = true;
				
				$smarty->assign("app_name", "RCA Store");

				self::$_instance = $smarty;
			}
			return self::$_instance;
		}				
	}

?>