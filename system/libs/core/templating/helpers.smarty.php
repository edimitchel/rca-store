<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type :     fonction
 * Nom :      cssuri
 * Rôle :     retourne le chemin vers le fichier css
 * -------------------------------------------------------------
 */
function smarty_function_cssuri($params, &$smarty)
{
    if(isset($params["name"])){
        $ext = isset($params["ext"]) ? $params["ext"] : false;
        if(isset($params["vendor"]) || isset($params["composer"]))
            return FrontController::getStyleUriFromVendor($params["name"], $ext);
        else
            return FrontController::getStyleUri($params["name"], $ext);
    }
}


/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type :     fonction
 * Nom :      importCss
 * Rôle :     import le fichier css (avec la balise link)
 * -------------------------------------------------------------
 */
function smarty_function_importCss($params, &$smarty)
{
    if(isset($params["name"])){
        $ext = isset($params["ext"]) ? $params["ext"] : false;
        if(isset($params["vendor"]) || isset($params["composer"]))
            return "<link rel=\"stylesheet\" href=\"" . FrontController::getStyleUriFromVendor($params["name"], $ext) . "\">";
        else
            return "<link rel=\"stylesheet\" href=\"" . FrontController::getStyleUri($params["name"], $ext) . "\">";
    }
}


/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type :     fonction
 * Nom :      jsuri
 * Rôle :     retourne le chemin vers le fichier js
 * -------------------------------------------------------------
 */
function smarty_function_jsuri($params, &$smarty)
{
    if(isset($params["name"])){
        $ext = isset($params["ext"]) ? $params["ext"] : false;
        if(isset($params["vendor"]) || isset($params["composer"]))
            return FrontController::getJsUriFromVendor($params["name"], $ext);
        else
            return FrontController::getJsUri($params["name"], $ext);
    }
}


/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type :     fonction
 * Nom :      importJs
 * Rôle :     import le fichier js (avec la balise script)
 * -------------------------------------------------------------
 */
function smarty_function_importJs($params, &$smarty)
{
    if(isset($params["name"])){
        $ext = isset($params["ext"]) ? $params["ext"] : false;
        if(isset($params["vendor"]) || isset($params["composer"]))
            return "<script src=\"" . FrontController::getJsUriFromVendor($params["name"], $ext) . "\"></script>";
        else
            return "<script src=\"" . FrontController::getJsUri($params["name"], $ext) . "\"></script>";
    }
}


/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type :     fonction
 * Nom :      imguri
 * Rôle :     retourne le chemin vers l'image
 * -------------------------------------------------------------
 */
function smarty_function_imguri($params, &$smarty)
{
    if(isset($params["name"])){
        $dir = isset($params["dir"]) ? $params["dir"] . '/' : "";
        return FrontController::getImageUri($dir . $params["name"]);
    }
    return false;
}


/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type :     fonction
 * Nom :      showImage
 * Rôle :     affiche l'image
 * -------------------------------------------------------------
 */
function smarty_function_showImage($params, &$smarty)
{
    $str = "<img src=\"" . smarty_function_imguri($params, $smarty) . "\"";
    if(isset($params['alt']))
        $str .= " alt=\"".$params['alt']."\"";
    $str .= ">";
    return $str;
}